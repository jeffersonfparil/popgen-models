### computing the recombination rate based on Clarke and Cardon (2005):https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1456135/pdf/GEN17142085.pdf
### with an additional trick of using the greatest distance among the most numberous pairs of distances then the computed recombination rate is divided by that distance
    
args = commandArgs(trailingOnly=TRUE)
# args = c("ACC62_combined-scaffold_11905|ref0020139.ld")
fname = args[1]
fname_out = sub("[.]ld", ".recom", fname)
neg_loglik  =  function(par, data){
    r = par[1]
    p_AB = data$p_AB; n_AB = data$n_AB
    p_ab = data$p_ab; n_ab = data$n_ab
    p_Ab = data$p_Ab; n_Ab = data$n_Ab
    p_aB = data$p_aB; n_aB = data$n_aB
    D = data$D
    loglik = -log(((p_ab - (D*r))^n_ab) *
                ((p_aB + (D*r))^n_aB) *
                ((p_Ab + (D*r))^n_Ab) *
                ((p_AB - (D*r))^n_AB))
    loglik = loglik[!is.infinite(loglik)]
    return(sum(loglik))
    }
ld = read.delim(fname, header=FALSE)
colnames(ld) = c("Location_of_SNP1", "Location_of_SNP2", "Number_of_pairs_observed_with_x_11", "Number_of_pairs_observed_with_x_12", "Number_of_pairs_observed_with_x_21", "Number_of_pairs_observed_with_x_22", "Estimate_for_allele_frequency_of_allele_A", "Estimate_for_allele_frequency_of_allele_B", "Read_depth_for_SNP1", "Read_depth_for_SNP2", "Intersecting_read_depth", "Approx_MLE_R2_(low_end_of_interval)", "Approx_MLE_estimate", "Approx_MLE_(high_end_of_interval)", "Direction_Computation_R2", "A", "a", "B", "b")
distance_bp = abs(ld$Location_of_SNP2 - ld$Location_of_SNP1)
r2 = ld$Approx_MLE_estimate
# plot(x=distance_bp, y = r2)
# ### find out which pair in terms of distance between each other, is most numerous and use them in tha calculation
# table_distance = table(distance_bp)
# selected_distance = as.numeric(as.character(tail(names(table_distance)[which(table_distance == max(table_distance), arr.ind=TRUE)], n=1)))
# ld_1bp = ld[distance_bp == selected_distance, ]
# if (nrow(ld_1bp)>0) {
#     n_ab = ld_1bp$Number_of_pairs_observed_with_x_22
#     n_aB = ld_1bp$Number_of_pairs_observed_with_x_21
#     n_Ab = ld_1bp$Number_of_pairs_observed_with_x_12
#     n_AB = ld_1bp$Number_of_pairs_observed_with_x_11
#     p_ab = n_ab / rowSums(cbind(n_ab, n_aB, n_Ab, n_AB))
#     p_aB = n_aB / rowSums(cbind(n_ab, n_aB, n_Ab, n_AB))
#     p_Ab = n_Ab / rowSums(cbind(n_ab, n_aB, n_Ab, n_AB))
#     p_AB = n_AB / rowSums(cbind(n_ab, n_aB, n_Ab, n_AB))
#     D = (p_ab * p_AB) - (p_aB * p_Ab)
#     data = data.frame(p_AB, p_ab, p_Ab, p_aB, n_AB, n_ab, n_Ab, n_aB, D)
#     r_bp = optim(par=c(0.5), fn=neg_loglik, data=data, method="Brent", lower=c(0), upper=c(1))$par / selected_distance
#     write.table(r_bp, file=fname_out, row.names=FALSE, col.names=FALSE, quote=FALSE)
# }
### estimate r_bp for all available loci pairs
list_r_bp = c()
for (bp in sort(unique(distance_bp))){
    # bp = sort(unique(distance_bp))[1]
    ld_1bp = ld[distance_bp==bp, ]
    n_ab = ld_1bp$Number_of_pairs_observed_with_x_22
    n_aB = ld_1bp$Number_of_pairs_observed_with_x_21
    n_Ab = ld_1bp$Number_of_pairs_observed_with_x_12
    n_AB = ld_1bp$Number_of_pairs_observed_with_x_11
    p_ab = n_ab / rowSums(cbind(n_ab, n_aB, n_Ab, n_AB))
    p_aB = n_aB / rowSums(cbind(n_ab, n_aB, n_Ab, n_AB))
    p_Ab = n_Ab / rowSums(cbind(n_ab, n_aB, n_Ab, n_AB))
    p_AB = n_AB / rowSums(cbind(n_ab, n_aB, n_Ab, n_AB))
    D = (p_ab * p_AB) - (p_aB * p_Ab)
    data = data.frame(p_AB, p_ab, p_Ab, p_aB, n_AB, n_ab, n_Ab, n_aB, D)
    r_bp = optim(par=c(0.5), fn=neg_loglik, data=data, method="Brent", lower=c(0), upper=c(1))$par / bp
    list_r_bp = c(list_r_bp, r_bp)
}
write.table(mean(list_r_bp), file=fname_out, row.names=FALSE, col.names=FALSE, quote=FALSE)
