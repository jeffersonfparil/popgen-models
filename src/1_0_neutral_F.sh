#!/bin/bash

# cd "/home/student.unimelb.edu.au/jparil/Documents/POPULATION_GENETICS-resistance_development_in_weed_populations/popgen-models/src/"
cd "/data/Lolium/Population_Genetics/POPGEN-MODELS"

### set source code directory
MS_DIR=/data/Lolium/Softwares/popgen-models/res
SRC_DIR=/data/Lolium/Softwares/popgen-models/src

### simulate neutral allele frequencies with ms (per herbicide resistance trait per scaffold)
### INPUTS:
###@@@ (1) npop
###@@@ (2) nind_per_pop
###@@@ (3) segsites


### simulate neutral allele frequencies
npop=60
nind_per_pop=1000
nind=$(echo "${npop} * ${nind_per_pop}" | bc)
nchrom=1
segsites=10000
time ${MS_DIR}/ms ${nind} ${nchrom} -s ${segsites} > ms.out
grep "positions" ms.out > pos.list
if [ -f geno_nxl_bin.csv ]
then 
    rm geno_nxl_bin.csv
else
    touch geno_nxl_bin.csv
fi
time \
for line in $(tail -n+7 ms.out)
do
    # line=$(tail -n+7 ms.out | head -n1)
    fold -w1 <(echo $line) | paste -sd, - >> geno_nxl_bin.csv
done
### took ~1 hour on 32-core 283-GB RAM machine [2020-10-27]
echo "n = rep(${nind_per_pop}, times=${npop})
      X = read.csv('geno_nxl_bin.csv', header=FALSE)
      FREQ = matrix(0, nrow=length(n), ncol=ncol(X))
      init = c(1, cumsum(n)[1:(length(n)-1)] + 1)
      slut = cumsum(n)
      for (i in 1:length(n)){
          FREQ[i, ] = colMeans(X[init[i]:slut[i], ])
      }
      write.table(FREQ, file='NEUTRAL_ALLELE_FREQ.csv', sep=',', row.names=FALSE, col.names=FALSE, quote=FALSE)
" > calc_freq.r
time Rscript calc_freq.r
### clean-up
rm seedms ms.out pos.list geno_nxl_bin.csv

### calculate variance-covariance matrix
### INPUTS:
###@@@ (1) Neutral allele frequency data (NEUTRAL_${herbi}.csv; NO HEADER; nPop x nLoci)
###@@@ (2) Filtered phenotype data (*-PHENOTYPES_DF.csv; HEADER:YEAR,SAMPLING,POP_GROUP,POP,NPOOLS,POOLSIZE,HERBICIDE,QUANTRES,SURVIVAL)
### OUTPUTS:
###@@@ (1) Neutral variance-covariance matrix (NEUTRAL_${herbi}-F_estimate.csv; NO HEADER; nPop x nPop)
time \
parallel \
Rscript ${SRC_DIR}/1_1_neutral_F.r \
    NEUTRAL_ALLELE_FREQ.csv \
    {1}-PHENOTYPES_DF.csv \
    {1} \
    ::: CLETH GLYPH SULFO TERBU
