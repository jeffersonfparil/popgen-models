################################################
### Vaiance-Covariance Matrix (F) estimation ###
################################################
### INPUTS:
###@@@ (1) Neutral allele frequency data (NEUTRAL_ALLELE_FREQ.csv; NO HEADER; nPop x nLoci)
###@@@ (2) Filtered phenotype data (*-PHENOTYPES_DF.csv; HEADER:YEAR,SAMPLING,POP_GROUP,POP,NPOOLS,POOLSIZE,HERBICIDE,QUANTRES,SURVIVAL)
### OUTPUTS:
###@@@ (1) Neutral variance-covariance matrix (NEUTRAL_${herbi}-F_estimate.csv; NO HEADER; nPop x nPop)
args = commandArgs(trailingOnly=TRUE)
# setwd("/home/student.unimelb.edu.au/jparil/Documents/POPULATION_GENETICS-resistance_development_in_weed_populations/popgen-models/src")
# args = c("NEUTRAL_ALLELE_FREQ.csv", 
#          "MERGED_GLYPH_MAPQ20_BASQ20_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT-PHENOTYPES_DF.csv",
#           "GLYPH")
fname_freq = args[1]
fname_phen = args[2]
herbi = args[3]
### load input dataframes
PHEN = read.csv(fname_phen)
FREQ = read.csv(fname_freq, header=FALSE)[1:nrow(PHEN),] ### slice to have compatible dimensions with the observed data (since these neutral allele frequencies are simulated using ms with arbitrarily large sizes)
### extract the vector of sample sizes * 2 (i.e. 2n)
m = PHEN$POOLSIZE * 2
# m = rep(100, times=nrow(PHEN))
### define the neutral F estimator function
FUNCTION_CALC_NEUTRAL_F = function(X, m){
    n = nrow(X)
    p = ncol(X)
    randomizer = runif(p) < 0.5
    X_rand = X
    X_rand[, randomizer] = 1-X_rand[, randomizer]
    M = (X_rand %*% t(X_rand)) / p
    diag(M) = (diag(M) * (m/(m-1))) - (rowMeans(X_rand)/(m-1))
    i = which(M == min(M,na.rm=TRUE), arr.ind=TRUE)[1,1]
    j = which(M == min(M,na.rm=TRUE), arr.ind=TRUE)[1,2]
    A = mean(X_rand[i, ] *      X_rand[j, ])
    C = mean(X_rand[i, ] * (1 - X_rand[j, ]))
    F = (M - A) / C
    return(F)
}
F = FUNCTION_CALC_NEUTRAL_F(X=as.matrix(FREQ), m=m)
fname_out = paste0("NEUTRAL_", herbi, "-F_estimate.csv")
write.table(F, file=fname_out, sep=",", row.names=FALSE, col.names=FALSE, quote=FALSE)
