########################################################################
###                                                                  ###
### CALCULATE VARIANCE-COVARIANCE MATRICES (F) OF ALLELE FREQUENCIES ###
###                                                                  ###
########################################################################

###########################
### Models being tested ###
###########################
###@@@ (1) Model 1: independent mutation model - independent mutation of favourbale allele across all resistant populations
###@@@ (2) Model 2: geneflow model - favourable allele is shared between all resistant populations via geneflow
###@@@ (3) Model 3: standing genetic variation model - favourable allele is shared between all resistant populations from a common ancestor
###@@@ (4) Model 4: geneflow and independent mutation model - some population/s share the favourable allele via geneflow; while the other population/s obtained the favourable allele on the same locus via de novo mutation
###@@@ (5) Model 5: standing genetic variation and independent mutation model - some population/s share the favourable from a common ancestor; while the other population/s obtained the favourable allele on the same locus via de novo mutation

###################
### Assumptions ###
###################
###@@@ (1) Favourable allele is fixed in all the resistant populations
###@@@ (2) This favourable allele fixation is recent
###@@@ (3) The same selection pressure across resistant populations
###@@@ (4) The same rate of geneflow across resistant populations or groups of resistant populations
###@@@ (5) The same mutation rate across resistant populations

##############
### Inputs ###
##############
args = commandArgs(trailingOnly=TRUE)
# args = c("ACC06,ACC08,ACC09,ACC11,ACC38,ACC50",
#          "1,5,6",
#          "(2,3),4",
#          "630887.333333333",
#          "0.00699322",
#          "CLETH-SELECTED_LOCI_DF-scaffold_1085|ref0000357.csv",
#          "CLETH-PHENOTYPES_DF.csv",
#          "NEUTRAL_CLETH-F_estimate.csv",
#          "100",
#          "/data/Lolium/Softwares/popgen-models/res/dmc",
#          "/data/Lolium/Population_Genetics/POPGEN-MODELS/CLETH--scaffold_1085|ref0000357--ACC06-ACC08-ACC09-ACC11-ACC38-ACC50--|2-3|-4")

pop_id = unlist(strsplit(args[1], ",")) ### comma-separated string of all ppopulations included in the analysis
idx_suscept = unlist(strsplit(args[2], ",")) ### comma-separated string of indices of susceptible populations (relating to pop_id above)
idx_resist_grouping = eval(parse(text=paste0("list(", gsub("[(]", "c(", args[3]), ")"))) ### indices of the resistant populations sharing common modes inside paranthesis and comma-separated
Ne = as.numeric(args[4]) ### arithmetic mean of effective population sizes across all populations
rec = as.numeric(args[5]) ### scaffold-specific arithmentic mean of per base pair recombination rate across all population
fname_scaffold_allele_freq = args[6] ### scaffold-specific comma-separated file of allele frequency data (WITH HEADER: CHROM,POS,POB_BP,ALLELE,ALPHA,LOD,{ALLELE_FREQUENCIES_PERP_POPULATION...}))
fname_phenotype = args[7] ### comma-spearated file of pehnotype data (WITH HEADER: YEAR,SAMPLING,POP_GROUP,POP,NPOOLS,POOLSIZE,HERBICIDE,QUANTRES,SURVIVAL)
fname_neutral_coancestry = args[8] ### comma-separated file of neutral (from ms-simulated data) coancestries (population pairwise) (NO HEADER: each population:1row:1col)
numBins = as.numeric(args[9]) ### number of bins to group alleles where the distance of each bin from the proposed selected site is calculated from the midpoint of this bin to the proposed selected site (numBins=100 since we simulated 1000 loci in ms and we want 10 alleles per bin)
DIR_DMC = args[10] ### directory where the dmc scripts are located (git clone https://github.com/kristinmlee/dmc)
DIR_OUT = args[11] ### output directory

############################
### Neutral expectations ###
############################
F_NEUTRAL = as.matrix(read.csv(fname_neutral_coancestry, header=FALSE))

#####################################
### Empirical data (observations) ###
#####################################
### load per scaffold loci
FREQ = read.csv(fname_scaffold_allele_freq)
### load phenotype data
PHEN = read.csv(fname_phenotype)
### extract only the data for the selected populations
idx_pop = c(1:nrow(PHEN))[PHEN$POP %in% pop_id]
F_estimate = F_NEUTRAL[idx_pop, idx_pop]
FREQ = FREQ[, c(1:6, idx_pop+6)]
PHEN = PHEN[idx_pop, ]
### SNP positions (mapped into the 0 to 1 range via minimum and maximum positions in bp)
# positions = FREQ$POS
positions = FREQ$POS_BP
### number of populations
numPops = length(pop_id)
### sample sizes per population
sampleSizes = PHEN$POOLSIZE
### indices of populations under selection
selPops = c(1:numPops)[!(c(1:numPops) %in% idx_suscept)]

#######################
### Parameter space ###
#######################
### proposed positions of selected sites
selSite = seq(min(positions), max(positions), length.out = 20)
### proposed selection coefficients (NOTE: assumes the same s for all populations under selection)
sels = c(1e-4, 1e-3, 0.01, seq(0.02, 0.14, by = 0.01), seq(0.15, 0.3, by = 0.05), seq(0.4, 0.6, by = 0.1))
### proposed number of generations prior to selection and migration
times = c(0, 5, 25, 50, 100, 500, 1000, 1e4, 1e6)
### proposed frequencies of the favorable allele for standing genetic variation
gs = c(1/(2*Ne), 10^-(4:1))
### proposed migration rates (i.e. proportion of individuals from the source population per generation; cannot be equal to zero)
migs = c(10^-(seq(5, 1, by = -2)), 0.5, 1)
### indices of sources of favorable allele
sources = selPops
### list of indices of resistant populations sharing the same mode of convergent adaptation
sets = idx_resist_grouping

##########################
### Models 1, 2, and 3 ###
##########################
### Calculate non-neutral coancestries of models with a single mode of convergent adaptation 
###@@@ (1) Model 1: independent mutation model - independent mutation of favourbale allele across all resistant populations
###@@@ (2) Model 2: geneflow model - favourable allele is shared between all resistant populations via geneflow
###@@@ (3) Model 3: standing genetic variation model - favourable allele is shared between all resistant populations from a common ancestor
###@@@ EXCLUDED: (4) Model 4: geneflow and independent mutation model - some population/s share the favourable allele via geneflow; while the other population/s obtained the favourable allele on the same locus via de novo mutation
###@@@ EXCLUDED: (5) Model 5: standing genetic variation and independent mutation model - some population/s share the favourable from a common ancestor; while the other population/s obtained the favourable allele on the same locus via de novo mutation

### Generate variance/covariance matrices (F^(S)) with single mode of convergent adaptation
source(paste0(DIR_DMC, "/genSelMatrices_individualModes.R"))

FOmegas_ind = lapply(sels, function(sel) {
  calcFOmegas_indSweeps(sel)
})
saveRDS(FOmegas_ind, paste0(DIR_OUT, "/FOmegas_ind_example.RDS"))

FOmegas_mig = lapply(sels ,function(sel) {
  lapply(migs, function(mig) {
    lapply(sources, function(my.source) {
      calcFOmegas_mig(sel, mig, my.source)
    })
  })
})
saveRDS(FOmegas_mig, paste0(DIR_OUT, "/FOmegas_mig_example.RDS"))

FOmegas_sv = lapply(sels, function(sel) {
  lapply(gs, function(g) {
    lapply(times, function(time) {
      lapply(sources, function(my.source) {
        calcFOmegas_stdVar.source(sel, g, time, my.source)
      })
    })
  })
})
saveRDS(FOmegas_sv, paste0(DIR_OUT, "/FOmegas_sv_example.RDS"))


#######################
### Models 4, and 5 ###
#######################
### Calculate non-neutral coancestries of models with multiple modes of convergent adaptation 
###@@@ EXCLUDED: (1) Model 1: independent mutation model - independent mutation of favourbale allele across all resistant populations
###@@@ EXCLUDED: (2) Model 2: geneflow model - favourable allele is shared between all resistant populations via geneflow
###@@@ EXCLUDED: (3) Model 3: standing genetic variation model - favourable allele is shared between all resistant populations from a common ancestor
###@@@ (4) Model 4: geneflow and independent mutation model - some population/s share the favourable allele via geneflow; while the other population/s obtained the favourable allele on the same locus via de novo mutation
###@@@ (5) Model 5: standing genetic variation and independent mutation model - some population/s share the favourable from a common ancestor; while the other population/s obtained the favourable allele on the same locus via de novo mutation

### Generate variance/covariance matrices (F^(S)) with multiple modes of convergent adaptation
source(paste0(DIR_DMC, "/genSelMatrices_multipleModes.R"))

### define the vector of modes corresponding to the populations sharing modes of covergent adaptation, sets
my.modes_migInd = c("mig", "ind") ### mig=migration model; ind=independent mutation, i.e. no migration

### times and gs are not involved in the migration model so we only loop over the first element of these vectors
FOmegas_mixed_migInd = lapply(sels ,function(sel) {
    lapply(gs[1], function(g) {
        lapply(times[1], function(time) {
            lapply(migs, function(mig) {
                lapply(sources, function(my.source) {
                    calcFOmegas_mixed(sel, g, time, mig, my.source, my.modes_migInd)
                })
            })
        })
    })
})
saveRDS(FOmegas_mixed_migInd, paste0(DIR_OUT, "/FOmegas_mixed_migInd_example.RDS"))

### define the vector of modes corresponding to the populations sharing modes of covergent adaptation, sets
my.modes_svInd = c("sv", "ind") ### sv=standing genetic variation; ind=independent mutation, i.e. no migration

### migs is not involved in the standing variant model so we only loop over the first element of this vector
FOmegas_mixed_svInd = lapply(sels ,function(sel) {
    lapply(gs, function(g) {
        lapply(times, function(time) {
            lapply(migs[1], function(mig) {
                lapply(sources, function(my.source) {
                    calcFOmegas_mixed(sel, g, time, mig, my.source, my.modes_svInd)
                })
            })
        })
    })
})
saveRDS(FOmegas_mixed_svInd, paste0(DIR_OUT, "/FOmegas_mixed_svInd_example.RDS"))
