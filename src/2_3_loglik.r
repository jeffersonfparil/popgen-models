######################################
###                                ###
### CALCULATE COMPOSITE LIKELIHOOD ###
###                                ###
######################################

###########################
### Models being tested ###
###########################
###@@@ (1) Model 1: independent mutation model - independent mutation of favourbale allele across all resistant populations
###@@@ (2) Model 2: geneflow model - favourable allele is shared between all resistant populations via geneflow
###@@@ (3) Model 3: standing genetic variation model - favourable allele is shared between all resistant populations from a common ancestor
###@@@ (4) Model 4: geneflow and independent mutation model - some population/s share the favourable allele via geneflow; while the other population/s obtained the favourable allele on the same locus via de novo mutation
###@@@ (5) Model 5: standing genetic variation and independent mutation model - some population/s share the favourable from a common ancestor; while the other population/s obtained the favourable allele on the same locus via de novo mutation

###################
### Assumptions ###
###################
###@@@ (1) Favourable allele is fixed in all the resistant populations
###@@@ (2) This favourable allele fixation is recent
###@@@ (3) The same selection pressure across resistant populations
###@@@ (4) The same rate of geneflow across resistant populations or groups of resistant populations
###@@@ (5) The same mutation rate across resistant populations

##############
### Inputs ###
##############
args = commandArgs(trailingOnly=TRUE)
# args = c("ACC06,ACC08,ACC09,ACC11,ACC38,ACC50",
#          "1,5,6",
#          "(2,3),4",
#          "630887.333333333",
#          "0.00699322",
#          "CLETH-SELECTED_LOCI_DF-scaffold_1085|ref0000357.csv",
#          "CLETH-PHENOTYPES_DF.csv",
#          "NEUTRAL_CLETH-F_estimate.csv",
#          "100",
#          "/data/Lolium/Softwares/popgen-models/res/dmc",
#          "/data/Lolium/Population_Genetics/POPGEN-MODELS/CLETH--scaffold_1085|ref0000357--ACC06-ACC08-ACC09-ACC11-ACC38-ACC50--|2-3|-4")

pop_id = unlist(strsplit(args[1], ",")) ### comma-separated string of all ppopulations included in the analysis
idx_suscept = unlist(strsplit(args[2], ",")) ### comma-separated string of indices of susceptible populations (relating to pop_id above)
idx_resist_grouping = eval(parse(text=paste0("list(", gsub("[(]", "c(", args[3]), ")"))) ### indices of the resistant populations sharing common modes inside paranthesis and comma-separated
Ne = as.numeric(args[4]) ### arithmetic mean of effective population sizes across all populations
rec = as.numeric(args[5]) ### scaffold-specific arithmentic mean of per base pair recombination rate across all population
fname_scaffold_allele_freq = args[6] ### scaffold-specific comma-separated file of allele frequency data (WITH HEADER: CHROM,POS,POB_BP,ALLELE,ALPHA,LOD,{ALLELE_FREQUENCIES_PERP_POPULATION...}))
fname_phenotype = args[7] ### comma-spearated file of pehnotype data (WITH HEADER: YEAR,SAMPLING,POP_GROUP,POP,NPOOLS,POOLSIZE,HERBICIDE,QUANTRES,SURVIVAL)
fname_neutral_coancestry = args[8] ### comma-separated file of neutral (from ms-simulated data) coancestries (population pairwise) (NO HEADER: each population:1row:1col)
numBins = as.numeric(args[9]) ### number of bins to group alleles where the distance of each bin from the proposed selected site is calculated from the midpoint of this bin to the proposed selected site (numBins=100 since we simulated 1000 loci in ms and we want 10 alleles per bin)
DIR_DMC = args[10] ### directory where the dmc scripts are located (git clone https://github.com/kristinmlee/dmc)
DIR_OUT = args[11] ### output directory

############################
### Neutral expectations ###
############################
F_NEUTRAL = as.matrix(read.csv(fname_neutral_coancestry, header=FALSE))

#####################################
### Empirical data (observations) ###
#####################################
### load per scaffold loci
FREQ = read.csv(fname_scaffold_allele_freq)
### load phenotype data
PHEN = read.csv(fname_phenotype)
### extract only the data for the selected populations
idx_pop = c(1:nrow(PHEN))[PHEN$POP %in% pop_id]
F_estimate = F_NEUTRAL[idx_pop, idx_pop]
FREQ = FREQ[, c(1:6, idx_pop+6)]
PHEN = PHEN[idx_pop, ]
### SNP positions (mapped into the 0 to 1 range via minimum and maximum positions in bp)
# positions = FREQ$POS
positions = FREQ$POS_BP
### number of populations
numPops = length(pop_id)
### sample sizes per population
sampleSizes = PHEN$POOLSIZE
### indices of populations under selection
selPops = c(1:numPops)[!(c(1:numPops) %in% idx_suscept)]

#######################
### Parameter space ###
#######################
### proposed positions of selected sites
selSite = seq(min(positions), max(positions), length.out = 20)
### proposed selection coefficients (NOTE: assumes the same s for all populations under selection)
sels = c(1e-4, 1e-3, 0.01, seq(0.02, 0.14, by = 0.01), seq(0.15, 0.3, by = 0.05), seq(0.4, 0.6, by = 0.1))
### proposed number of generations prior to selection and migration
times = c(0, 5, 25, 50, 100, 500, 1000, 1e4, 1e6)
### proposed frequencies of the favorable allele for standing genetic variation
gs = c(1/(2*Ne), 10^-(4:1))
### proposed migration rates (i.e. proportion of individuals from the source population per generation; cannot be equal to zero)
migs = c(10^-(seq(5, 1, by = -2)), 0.5, 1)
### indices of sources of favorable allele
sources = selPops
### list of indices of resistant populations sharing the same mode of convergent adaptation
sets = idx_resist_grouping



####################################################################################################
### inverses and determinants of all the F matrices: models neutral, 1, 2, 3, 4, and 5
####################################################################################################

library("MASS")

M = numPops
Tmatrix = matrix(data = rep(-1 / M, (M - 1) * M), nrow = M - 1, ncol = M)
diag(Tmatrix) = (M - 1) / M 

sampleErrorMatrix = diag(1/sampleSizes, nrow = numPops, ncol = numPops)

det_FOmegas_neutral = det(Tmatrix %*% (F_estimate + sampleErrorMatrix) %*% t(Tmatrix))
saveRDS(det_FOmegas_neutral, paste0(DIR_OUT, "/det_FOmegas_neutral_example.RDS"))

inv_FOmegas_neutral = ginv(Tmatrix %*% (F_estimate + sampleErrorMatrix) %*% t(Tmatrix))
saveRDS(inv_FOmegas_neutral, paste0(DIR_OUT, "/inv_FOmegas_neutral_example.RDS"))



## Model 1
FOmegas_ind = readRDS(paste0(DIR_OUT, "/FOmegas_ind_example.RDS"))

det_FOmegas_ind = lapply(FOmegas_ind, function(sel) {
    lapply(sel, function(dist) {
        det(dist)
    })
})
saveRDS(det_FOmegas_ind, paste0(DIR_OUT, "/det_FOmegas_ind_example.RDS"))

inv_FOmegas_ind = lapply(FOmegas_ind, function(sel) {
    lapply(sel, function(dist) {
        ginv(dist)
    })
})
saveRDS(inv_FOmegas_ind, paste0(DIR_OUT, "/inv_FOmegas_ind_example.RDS"))



## Model 2
FOmegas_mig = readRDS(paste0(DIR_OUT, "/FOmegas_mig_example.RDS"))

det_FOmegas_mig_output = lapply(FOmegas_mig, function(sel) {
    lapply(sel, function(mig) {
        lapply(mig, function(source) {
            lapply(source, function(dist) {
                det(dist)
            })
        })
    })
})
saveRDS(det_FOmegas_mig_output, paste0(DIR_OUT, "/det_FOmegas_mig_example.RDS"))

inv_FOmegas_mig_output = lapply(FOmegas_mig, function(sel) {
    lapply(sel, function(mig) {
        lapply(mig, function(source) {
            lapply(source, function(dist) {
                ginv(dist)
            })
        })
    })
})
saveRDS(inv_FOmegas_mig_output, paste0(DIR_OUT, "/inv_FOmegas_mig_example.RDS"))



## Model 3
FOmegas_sv = readRDS(paste0(DIR_OUT, "/FOmegas_sv_example.RDS"))

det_FOmegas_sv = lapply(FOmegas_sv, function(sel) {
    lapply(sel, function(g) {
        lapply(g, function(time) {
            lapply(time, function(my.source) {
                lapply(my.source, function(dist) {
                    det(dist)
                })
            })
        })
    })
})
saveRDS(det_FOmegas_sv, paste0(DIR_OUT, "/det_FOmegas_sv_example.RDS"))

inv_FOmegas_sv = lapply(FOmegas_sv, function(sel) {
    lapply(sel, function(g) {
        lapply(g, function(time) {
            lapply(time, function(my.source) {
                lapply(my.source, function(dist) {
                    ginv(dist)
                })
            })
        })
    })
})
saveRDS(inv_FOmegas_sv, paste0(DIR_OUT, "/inv_FOmegas_sv_example.RDS"))

## Model 4
FOmegas_mixed_migInd = readRDS(paste0(DIR_OUT, "/FOmegas_mixed_migInd_example.RDS"))

detFOmegas_mixed_migInd = lapply(FOmegas_mixed_migInd, function(sel) {
    lapply(sel, function(g) {
        lapply(g, function(time) {
            lapply(time, function(mig) {
                lapply(mig, function(source) {
                    lapply(source, function(dist) {
                        det(dist)
                    })
                })  
            })
        })
    })
})
saveRDS(detFOmegas_mixed_migInd, paste0(DIR_OUT, "/det_FOmegas_mixed_migInd_example.RDS"))

invFOmegas_mixed_migInd = lapply(FOmegas_mixed_migInd, function(sel) {
    lapply(sel, function(g) {
        lapply(g, function(time) {
            lapply(time, function(mig) {
                lapply(mig, function(source) {
                    lapply(source, function(dist) {
                        ginv(dist)
                    })
                })  
            })
        })
    })
})
saveRDS(invFOmegas_mixed_migInd, paste0(DIR_OUT, "/inv_FOmegas_mixed_migInd_example.RDS"))

## Model 5
FOmegas_mixed_svInd = readRDS(paste0(DIR_OUT, "/FOmegas_mixed_svInd_example.RDS"))

detFOmegas_mixed_svInd = lapply(FOmegas_mixed_svInd, function(sel) {
    lapply(sel, function(g) {
        lapply(g, function(time) {
            lapply(time, function(mig) {
                lapply(mig, function(source) {
                    lapply(source, function(dist) {
                        det(dist)
                    })
                })  
            })
        })
    })
})
saveRDS(detFOmegas_mixed_svInd, paste0(DIR_OUT, "/det_FOmegas_mixed_svInd_example.RDS"))

invFOmegas_mixed_svInd = lapply(FOmegas_mixed_svInd, function(sel) {
    lapply(sel, function(g) {
        lapply(g, function(time) {
            lapply(time, function(mig) {
                lapply(mig, function(source) {
                    lapply(source, function(dist) {
                        ginv(dist)
                    })
                })  
            })
        })
    })
})
saveRDS(invFOmegas_mixed_svInd, paste0(DIR_OUT, "/inv_FOmegas_mixed_svInd_example.RDS"))





####################################################################################################
### finally the composite log-likelihoods of the all F: models neutral, 1, 2, 3, 4, and 5
####################################################################################################


# freqs_notRand = readRDS("example/selectedRegionAlleleFreqs_example.RDS")
freqs_notRand = t(FREQ[,7:ncol(FREQ)])

randFreqs = apply(freqs_notRand, 2, function(my.freqs) {
    if(runif(1) < 0.5) {
        my.freqs = 1 - my.freqs
    }
    my.freqs
})

saveRDS(randFreqs, paste0(DIR_OUT, "/selectedRegionAlleleFreqsRand_example.RDS"))

# numPops = 6
# positions = readRDS("example/selectedRegionPositions_example.RDS")

freqs = readRDS(paste0(DIR_OUT, "/selectedRegionAlleleFreqsRand_example.RDS"))

#these values must be same as used to calculate Calculate F^(S) matrices above
# numBins = 1000
# selSite = seq(min(positions), max(positions), length.out = 10)
# sels = c(1e-4, 1e-3, 0.01, seq(0.02, 0.14, by = 0.01), seq(0.15, 0.3, by = 0.05), 
#          seq(0.4, 0.6, by = 0.1))
# times = c(0, 5, 25, 50, 100, 500, 1000, 1e4, 1e6)
# Ne = 10000 #only necessary to specify since we define lowest value of g by Ne
# gs = c(1/(2*Ne), 10^-(4:1))
# migs = c(10^-(seq(5, 1, by = -2)), 0.5, 1)
# selPops = c(1, 3, 5) 
# #only necessary to specify since we define possible source populations as all
# ## selected populations
# sources = selPops

source(paste0(DIR_DMC, "/calcCompositeLike.R"))

## Neutral model
det_FOmegas_neutral = readRDS(paste0(DIR_OUT, "/det_FOmegas_neutral_example.RDS"))
inv_FOmegas_neutral = readRDS(paste0(DIR_OUT, "/inv_FOmegas_neutral_example.RDS"))

compLikelihood_neutral = lapply(1 : length(selSite), function(j) {
  calcCompLikelihood_neutral(j, det_FOmegas_neutral, inv_FOmegas_neutral)})

saveRDS(compLikelihood_neutral, paste0(DIR_OUT, "/compLikelihood_neutral_example.RDS"))

## Model 1
det_FOmegas_ind = readRDS(paste0(DIR_OUT, "/det_FOmegas_ind_example.RDS"))
inv_FOmegas_ind = readRDS(paste0(DIR_OUT, "/inv_FOmegas_ind_example.RDS"))
compLikelihood_ind = lapply(1 : length(selSite), function(j) {
  lapply(1 : length(sels), function(sel) calcCompLikelihood_1par(j, det_FOmegas_ind,
                                                                 inv_FOmegas_ind, sel))
})
saveRDS(compLikelihood_ind, paste0(DIR_OUT, "/compLikelihood_ind_example.RDS"))

## Model 2
det_FOmegas_mig = readRDS(paste0(DIR_OUT, "/det_FOmegas_mig_example.RDS"))
inv_FOmegas_mig = readRDS(paste0(DIR_OUT, "/inv_FOmegas_mig_example.RDS"))
compLikelihood_mig = lapply(1 : length(selSite), function(j) {
    lapply(1 : length(sels), function(sel) {
        lapply(1 : length(migs), function(mig) {
            lapply(1 : length(sources), function(my.source) {
                calcCompLikelihood_3par(j, det_FOmegas_mig, inv_FOmegas_mig, sel, mig,
                                        my.source)
            })
        })
    })
})
saveRDS(compLikelihood_mig, paste0(DIR_OUT, "/compLikelihood_mig_example.RDS"))

## Model 3
det_FOmegas_sv = readRDS(paste0(DIR_OUT, "/det_FOmegas_sv_example.RDS"))
inv_FOmegas_sv = readRDS(paste0(DIR_OUT, "/inv_FOmegas_sv_example.RDS"))
compLikelihood_sv = lapply(1 : length(selSite), function(j) {
    lapply(1 : length(sels), function(sel) {
        lapply(1 : length(gs), function(g) {
            lapply(1 : length(times), function(t) {
                lapply(1: length(sources), function(my.source) {
                calcCompLikelihood_4par(j, det_FOmegas_sv, inv_FOmegas_sv, sel, g, t,
                                        my.source)
                })
            })
        })
    })
})
saveRDS(compLikelihood_sv, paste0(DIR_OUT, "/compLikelihood_sv_example.RDS"))

## Model 4
det_FOmegas_mixed_migInd = readRDS(paste0(DIR_OUT, "/det_FOmegas_mixed_migInd_example.RDS"))
inv_FOmegas_mixed_migInd = readRDS(paste0(DIR_OUT, "/inv_FOmegas_mixed_migInd_example.RDS"))

# same trick as above (the parameters time and g are not involved in the migration
## model so we only loop over the first element of these vectors)
# now save lists for each proposed selected site (may want to do this for other 
## models/more elegantly depending on density of parameter space)
for(j in 1 : length(selSite)) {
    compLikelihood_mixed_migInd = lapply(1 : length(sels), function(sel) {
        lapply(1 : length(gs[1]), function(g) {
            lapply(1 : length(times[1]), function(t) {
              lapply(1 : length(migs), function (mig) {
                  lapply(1: length(sources), function(my.source) {
                    calcCompLikelihood_5par(j, det_FOmegas_mixed_migInd,
                                            inv_FOmegas_mixed_migInd, sel, g, t, mig,
                                            my.source)
                  })
                })
            })
        })
    })
  saveRDS(compLikelihood_mixed_migInd,
          paste0(DIR_OUT, "/compLikelihood_mixed_migInd_example_selSite", j, ".RDS"))
}

## Model 5
det_FOmegas_mixed_svInd = readRDS(paste0(DIR_OUT, "/det_FOmegas_mixed_svInd_example.RDS"))
inv_FOmegas_mixed_svInd = readRDS(paste0(DIR_OUT, "/inv_FOmegas_mixed_svInd_example.RDS"))

#same trick as above (the parameter mig is not involved in the migration model so we
##only loop over the first element of this vector)
# now save lists for each proposed selected site (may want to do this for other
## models/more elegantly depending on density of parameter space)
for(j in 1 : length(selSite)) {
    compLikelihood_mixed_svInd = lapply(1 : length(sels), function(sel) {
        lapply(1 : length(gs), function(g) {
            lapply(1 : length(times), function(t) {
              lapply(1 : length(migs[1]), function (mig) {
                  lapply(1: length(sources), function(my.source) {
                    calcCompLikelihood_5par(j, det_FOmegas_mixed_svInd,
                                            inv_FOmegas_mixed_svInd, sel, g, t, mig,
                                            my.source)
                  })
                })
            })
        })
    })
  saveRDS(compLikelihood_mixed_svInd,
          paste0(DIR_OUT, "/compLikelihood_mixed_svInd_example_selSite", j, ".RDS"))
}
