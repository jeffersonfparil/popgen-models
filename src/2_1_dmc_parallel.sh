#!/bin/bash

### PARALLELIZABLE SCRIPT

###########################################################################################
### COALESCENT-BASED LIKELIHOOD ESTIMATION OF DIFFERENT MODELS OF CONVERGENT ADAPTATION ###
###########################################################################################
### Using the composite-likelihood-based approach of Lee and Coop (2017)
### https://www.genetics.org/content/207/4/1591

###########################
### Models being tested ###
###########################
###@@@ (1) Model 1: independent mutation model - independent mutation of favourbale allele across all resistant populations
###@@@ (2) Model 2: geneflow model - favourable allele is shared between all resistant populations via geneflow
###@@@ (3) Model 3: standing genetic variation model - favourable allele is shared between all resistant populations from a common ancestor
###@@@ (4) Model 4: geneflow and independent mutation model - some population/s share the favourable allele via geneflow; while the other population/s obtained the favourable allele on the same locus via de novo mutation
###@@@ (5) Model 5: standing genetic variation and independent mutation model - some population/s share the favourable from a common ancestor; while the other population/s obtained the favourable allele on the same locus via de novo mutation

###################
### Assumptions ###
###################
###@@@ (1) Favourable allele is fixed in all the resistant populations
###@@@ (2) This favourable allele fixation is recent
###@@@ (3) The same selection pressure across resistant populations
###@@@ (4) The same rate of geneflow across resistant populations or groups of resistant populations
###@@@ (5) The same mutation rate across resistant populations

##############
### INPUTS ###
##############
###@@@ (1) Line number pointing to the entry in the input tab-delimited file listing the input files, Ne, recombination rates, etc (see below next input item for details)
###@@@ (2) Tab-delimited file: "input_fnames_params.txt" with the following field and header names:
###@@@     HERBI, SCAFFOLD, FNAME_FREQ_PER_SCAFFOLD, FNAME_PHENO, FNAME_F_ESTIMATE, POPS, ID_SUCEPT, ID_RESIST_GROUPING, NE, RECOM, NUMBINS
###@@@ (3) working directory where all the input files are located
###@@@ (4) source code directory of the popgen-models.git repo, e.g. "/data/Lolium/Softwares/popgen-models/src"
line=$1
input_fnames_params=$2
DIR=$3
SRC_DIR=$4

### TESTS:
# line=8
# input_fnames_params=input_fnames_params.txt
# DIR="/data/Lolium/Population_Genetics/POPGEN-MODELS"
# SRC_DIR="/data/Lolium/Softwares/popgen-models/src"

### navigate into the working directory
cd $DIR
### directory where the dmc scripts are located (git clone https://github.com/kristinmlee/dmc)
DMC_DIR=${SRC_DIR%/src*}/res/dmc
### line containing the input details
input=$(head -n${line} ${input_fnames_params} | tail -n1)
### herbicide resistance trait
herbi=$(cut -d" " -f1 <(echo $input))
### scaffold to use
scaffold=$(cut -d" " -f2 <(echo $input))
### comma-separated string of all ppopulations included in the analysis
pops=$(cut -d" " -f6 <(echo $input))
### comma-separated string of indices of susceptible populations (relating to pop_id above)
id_suscept=$(cut -d" " -f7 <(echo $input))
### indices of the resistant populations sharing common modes inside paranthesis and comma-separated
id_resist_grouping=$(cut -d" " -f8 <(echo $input))
### arithmetic mean of effective population sizes across all populations
Ne=$(cut -d" " -f9 <(echo $input))
### scaffold-specific arithmentic mean of per base pair recombination rate across all population
recom=$(cut -d" " -f10 <(echo $input))
### scaffold-specific comma-separated file of allele frequency data (WITH HEADER: CHROM,POS,POB_BP,ALLELE,ALPHA,LOD,{ALLELE_FREQUENCIES_PERP_POPULATION...}))
FNAME_FREQ_PER_SCAFFOLD=$(cut -d" " -f3 <(echo $input))
### comma-spearated file of pehnotype data (WITH HEADER: YEAR,SAMPLING,POP_GROUP,POP,NPOOLS,POOLSIZE,HERBICIDE,QUANTRES,SURVIVAL)
FNAME_PHENO=$(cut -d" " -f4 <(echo $input))
### comma-separated file of neutral (from ms-simulated data) coancestries (population pairwise) (NO HEADER: each population:1row:1col)
FNAME_F_ESTIMATE=$(cut -d" " -f5 <(echo $input))
### number of bins to group alleles where the distance of each bin from the proposed selected site is calculated from the midpoint of this bin to the proposed selected site (numBins=100 since we simulated 1000 loci in ms and we want 10 alleles per bin)
numBins=$(cut -d" " -f11 <(echo $input))

####################
### MAIN OUTPUTS ###
####################
###@@@ (1) ${OUT_NEW_PREFIX}.csv - parameter estimates of the most likely model (HEADER: model, maxLoc, maxSel, maxG, maxTime, maxMig, maxSource)
###@@@ (2) ${OUT_NEW_PREFIX}.svg - likelihood profile and manhattan plot across the scaffold
OUT_PREFIX=${FNAME_FREQ_PER_SCAFFOLD%.csv*}--$(sed "s/,/-/g" <(echo $pops))--$(sed "s/(/|/g" <(sed "s/)/|/g" <(sed "s/,/-/g" <(echo $id_resist_grouping))))
OUT_NEW_PREFIX=${herbi}--${scaffold}--$(sed "s/,/-/g" <(echo $pops))--$(sed "s/(/|/g" <(sed "s/)/|/g" <(sed "s/,/-/g" <(echo $id_resist_grouping))))

### output directory
mkdir ${DIR}/${OUT_NEW_PREFIX}
OUT_DIR=${DIR}/${OUT_NEW_PREFIX}
echo $OUT_DIR

time \
Rscript ${SRC_DIR}/2_2_F.r ${pops} \
                           ${id_suscept} \
                           ${id_resist_grouping} \
                           ${Ne} \
                           ${recom} \
                           ${FNAME_FREQ_PER_SCAFFOLD} \
                           ${FNAME_PHENO} \
                           ${FNAME_F_ESTIMATE} \
                           ${numBins} \
                           ${DMC_DIR} \
                           ${OUT_DIR}
time \
Rscript ${SRC_DIR}/2_3_loglik.r ${pops} \
                                ${id_suscept} \
                                ${id_resist_grouping} \
                                ${Ne} \
                                ${recom} \
                                ${FNAME_FREQ_PER_SCAFFOLD} \
                                ${FNAME_PHENO} \
                                ${FNAME_F_ESTIMATE} \
                                ${numBins} \
                                ${DMC_DIR} \
                                ${OUT_DIR}
time \
Rscript ${SRC_DIR}/2_4_assess.r ${pops} \
                                ${id_suscept} \
                                ${id_resist_grouping} \
                                ${Ne} \
                                ${recom} \
                                ${FNAME_FREQ_PER_SCAFFOLD} \
                                ${FNAME_PHENO} \
                                ${FNAME_F_ESTIMATE} \
                                ${numBins} \
                                ${DMC_DIR} \
                                ${OUT_DIR}

### clean-up
cd ${OUT_DIR}
# rm *.RDS
mv ${OUT_PREFIX}.csv ${OUT_NEW_PREFIX}.csv
if [ -f ${OUT_PREFIX}.svg ]
then
    mv ${OUT_PREFIX}.svg ${OUT_NEW_PREFIX}.svg ### may not exists if ${OUT_NEW_PREFIX}.csv==NAs
fi