#########################
###                   ###
### MODEL ASSESMENNTS ###
###                   ###
#########################

###########################
### Models being tested ###
###########################
###@@@ (1) Model 1: independent mutation model - independent mutation of favourbale allele across all resistant populations
###@@@ (2) Model 2: geneflow model - favourable allele is shared between all resistant populations via geneflow
###@@@ (3) Model 3: standing genetic variation model - favourable allele is shared between all resistant populations from a common ancestor
###@@@ (4) Model 4: geneflow and independent mutation model - some population/s share the favourable allele via geneflow; while the other population/s obtained the favourable allele on the same locus via de novo mutation
###@@@ (5) Model 5: standing genetic variation and independent mutation model - some population/s share the favourable from a common ancestor; while the other population/s obtained the favourable allele on the same locus via de novo mutation

###################
### Assumptions ###
###################
###@@@ (1) Favourable allele is fixed in all the resistant populations
###@@@ (2) This favourable allele fixation is recent
###@@@ (3) The same selection pressure across resistant populations
###@@@ (4) The same rate of geneflow across resistant populations or groups of resistant populations
###@@@ (5) The same mutation rate across resistant populations

##############
### Inputs ###
##############
args = commandArgs(trailingOnly=TRUE)
# args = c("ACC06,ACC08,ACC09,ACC11,ACC38,ACC50",
#          "1,5,6",
#          "(2,3),4",
#          "630887.333333333",
#          "0.00699322",
#          "CLETH-SELECTED_LOCI_DF-scaffold_1085|ref0000357.csv",
#          "CLETH-PHENOTYPES_DF.csv",
#          "NEUTRAL_CLETH-F_estimate.csv",
#          "100",
#          "/data/Lolium/Softwares/popgen-models/res/dmc",
#          "/data/Lolium/Population_Genetics/POPGEN-MODELS/CLETH--scaffold_1085|ref0000357--ACC06-ACC08-ACC09-ACC11-ACC38-ACC50--|2-3|-4")

pop_id = unlist(strsplit(args[1], ",")) ### comma-separated string of all ppopulations included in the analysis
idx_suscept = unlist(strsplit(args[2], ",")) ### comma-separated string of indices of susceptible populations (relating to pop_id above)
idx_resist_grouping = eval(parse(text=paste0("list(", gsub("[(]", "c(", args[3]), ")"))) ### indices of the resistant populations sharing common modes inside paranthesis and comma-separated
Ne = as.numeric(args[4]) ### arithmetic mean of effective population sizes across all populations
rec = as.numeric(args[5]) ### scaffold-specific arithmentic mean of per base pair recombination rate across all population
fname_scaffold_allele_freq = args[6] ### scaffold-specific comma-separated file of allele frequency data (WITH HEADER: CHROM,POS,POB_BP,ALLELE,ALPHA,LOD,{ALLELE_FREQUENCIES_PERP_POPULATION...}))
fname_phenotype = args[7] ### comma-spearated file of pehnotype data (WITH HEADER: YEAR,SAMPLING,POP_GROUP,POP,NPOOLS,POOLSIZE,HERBICIDE,QUANTRES,SURVIVAL)
fname_neutral_coancestry = args[8] ### comma-separated file of neutral (from ms-simulated data) coancestries (population pairwise) (NO HEADER: each population:1row:1col)
numBins = as.numeric(args[9]) ### number of bins to group alleles where the distance of each bin from the proposed selected site is calculated from the midpoint of this bin to the proposed selected site (numBins=100 since we simulated 1000 loci in ms and we want 10 alleles per bin)
DIR_DMC = args[10] ### directory where the dmc scripts are located (git clone https://github.com/kristinmlee/dmc)
DIR_OUT = args[11] ### output directory

####################
### Main Outputs ###
####################
###@@@ (1) OUT_FNAME_PREFIX, ".csv") - parameter estimates of the most likely model (HEADER: model, maxLoc, maxSel, maxG, maxTime, maxMig, maxSource)
###@@@ (2) OUT_FNAME_PREFIX, ".svg") - likelihood profile and manhattan plot across the scaffold
###@@@ where:
OUT_FNAME_PREFIX = paste(gsub(".csv", "", basename(fname_scaffold_allele_freq)),
                         paste(pop_id,collapse="-"),
                         gsub("[(]", "|", gsub("[)]", "|", gsub(",", "-", args[3]))),
                         sep="--")

############################
### Neutral expectations ###
############################
F_NEUTRAL = as.matrix(read.csv(fname_neutral_coancestry, header=FALSE))

#####################################
### Empirical data (observations) ###
#####################################
### load per scaffold loci
FREQ = read.csv(fname_scaffold_allele_freq)
### load phenotype data
PHEN = read.csv(fname_phenotype)
### extract only the data for the selected populations
idx_pop = c(1:nrow(PHEN))[PHEN$POP %in% pop_id]
F_estimate = F_NEUTRAL[idx_pop, idx_pop]
FREQ = FREQ[, c(1:6, idx_pop+6)]
PHEN = PHEN[idx_pop, ]
### SNP positions (mapped into the 0 to 1 range via minimum and maximum positions in bp)
# positions = FREQ$POS
positions = FREQ$POS_BP
### number of populations
numPops = length(pop_id)
### sample sizes per population
sampleSizes = PHEN$POOLSIZE
### indices of populations under selection
selPops = c(1:numPops)[!(c(1:numPops) %in% idx_suscept)]

#######################
### Parameter space ###
#######################
### proposed positions of selected sites
selSite = seq(min(positions), max(positions), length.out = 20)
### proposed selection coefficients (NOTE: assumes the same s for all populations under selection)
sels = c(1e-4, 1e-3, 0.01, seq(0.02, 0.14, by = 0.01), seq(0.15, 0.3, by = 0.05), seq(0.4, 0.6, by = 0.1))
### proposed number of generations prior to selection and migration
times = c(0, 5, 25, 50, 100, 500, 1000, 1e4, 1e6)
### proposed frequencies of the favorable allele for standing genetic variation
gs = c(1/(2*Ne), 10^-(4:1))
### proposed migration rates (i.e. proportion of individuals from the source population per generation; cannot be equal to zero)
migs = c(10^-(seq(5, 1, by = -2)), 0.5, 1)
### indices of sources of favorable allele
sources = selPops
### list of indices of resistant populations sharing the same mode of convergent adaptation
sets = idx_resist_grouping





######################################################################################
### Determine the maximum composite log-likelihood for all proposed selected sites ###
######################################################################################
### Neutral expectation
compLikelihood_neutral = readRDS(paste0(DIR_OUT, "/compLikelihood_neutral_example.RDS"))
compLikelihood_neutral_site = sapply(1 : length(selSite), function(i) {
  max(unlist(compLikelihood_neutral[[i]]))
})
## Model 1
compLikelihood_ind = readRDS(paste0(DIR_OUT, "/compLikelihood_ind_example.RDS"))
compLikelihood_ind_site = sapply(1 : length(selSite), function(i) {
  max(unlist(compLikelihood_ind[[i]]))
})
## Model 2
compLikelihood_mig = readRDS(paste0(DIR_OUT, "/compLikelihood_mig_example.RDS"))
compLikelihood_mig_site = sapply(1 : length(selSite), function(i) {
  max(unlist(compLikelihood_mig[[i]]))
})
## Model 3
compLikelihood_sv = readRDS(paste0(DIR_OUT, "/compLikelihood_sv_example.RDS"))
compLikelihood_sv_site = sapply(1 : length(selSite), function(i) {  
  max(unlist(compLikelihood_sv[[i]]))
})
## Model 4
compLikelihood_mixed_migInd_all = lapply(1: length(selSite), function(i) {
  readRDS(paste0(DIR_OUT, "/compLikelihood_mixed_migInd_example_selSite", i, ".RDS"))
})
saveRDS(compLikelihood_mixed_migInd_all, paste0(DIR_OUT, "/compLikelihood_mixed_migInd_example.RDS"))
compLikelihood_mixed_migInd = readRDS(paste0(DIR_OUT, "/compLikelihood_mixed_migInd_example.RDS"))
compLikelihood_mixed_migInd_site = sapply(1 : length(selSite), function(i) {
  max(unlist(compLikelihood_mixed_migInd[[i]]))
})
## Model 5
compLikelihood_mixed_svInd_all = lapply(1: length(selSite), function(i) {
  readRDS(paste0(DIR_OUT, "/compLikelihood_mixed_svInd_example_selSite", i, ".RDS"))
})
saveRDS(compLikelihood_mixed_svInd_all, paste0(DIR_OUT, "/compLikelihood_mixed_svInd_example.RDS"))
compLikelihood_mixed_svInd = readRDS(paste0(DIR_OUT, "/compLikelihood_mixed_svInd_example.RDS"))
compLikelihood_mixed_svInd_site = sapply(1 : length(selSite), function(i) {
  max(unlist(compLikelihood_mixed_svInd[[i]]))
})

############################################################################################
### Identify the model with the highest mean-max-comp-log-likelihood across the scaffold ###
############################################################################################
MODEL1 = mean(compLikelihood_ind_site - compLikelihood_neutral_site)
MODEL2 = mean(compLikelihood_mig_site - compLikelihood_neutral_site)
MODEL3 = mean(compLikelihood_sv_site - compLikelihood_neutral_site)
MODEL4 = mean(compLikelihood_mixed_migInd_site - compLikelihood_neutral_site)
MODEL5 = mean(compLikelihood_mixed_svInd_site - compLikelihood_neutral_site)
MODEL_MAX_NAME = tryCatch(c("MODEL1", "MODEL2", "MODEL3", "MODEL4", "MODEL5")[which(c(MODEL1, MODEL2, MODEL3, MODEL4, MODEL5)==max(c(MODEL1, MODEL2, MODEL3, MODEL4, MODEL5),na.rm=TRUE))][1],
                          error=function(e){
                            NA;
                            print("Something went wrong. Please check the composite log-likelihood calculations.")
                          }
                         )

#### Extract the maximum composite likelihood estimates of the parameters
source(paste0(DIR_DMC, "/getMCLE.R"))

if (is.na(MODEL_MAX_NAME)==TRUE){
  model     = c(NA) ### model name
  muLogLik  = c(NA) ### mean MODEL - NEUTRAL -log-likelihood
  maxLoc    = c(NA) ### most likely location of the selected site
  maxSel    = c(NA) ### most likely selection coefficient across all selected populations
  maxG      = c(NA) ### most likely standing favourable allele frequency
  maxTime   = c(NA) ### most likely time since the alleles have been in the population/s
  maxMig    = c(NA) ### most likely migration rate
  maxSource = c(NA) ###most likely source population
} else {
  ## Model 1
  max_comp_log_lik_est = getMCLEind(compLikelihood_ind, selSite, sels)
  model     = c("MODEL1")
  muLogLik  = c(MODEL1)
  maxLoc    = c(max_comp_log_lik_est[,1])
  maxSel    = c(max_comp_log_lik_est[,2])
  maxG      = c(NA)
  maxTime   = c(NA)
  maxMig    = c(NA)
  maxSource = c(NA)
  ## Model 2
  max_comp_log_lik_est = getMCLEmig(compLikelihood_mig, selSite, sels, migs, sources)
  model     = c(model, "MODEL2")
  muLogLik  = c(muLogLik, MODEL2)
  maxLoc    = c(maxLoc, max_comp_log_lik_est[,1])
  maxSel    = c(maxSel, max_comp_log_lik_est[,2])
  maxG      = c(maxG, NA)
  maxTime   = c(maxTime, NA)
  maxMig    = c(maxMig, max_comp_log_lik_est[,3])
  maxSource = c(maxSource, max_comp_log_lik_est[,4])
  ## Model 3
  max_comp_log_lik_est = getMCLEsv_source(compLikelihood_sv, selSite, sels, gs, times, sources)
  model     = c(model, "MODEL3")
  muLogLik  = c(muLogLik, MODEL3)
  maxLoc    = c(maxLoc, max_comp_log_lik_est[,1])
  maxSel    = c(maxSel, max_comp_log_lik_est[,2])
  maxG      = c(maxG, max_comp_log_lik_est[,3])
  maxTime   = c(maxTime, max_comp_log_lik_est[,4])
  maxMig    = c(maxMig, NA)
  maxSource = c(maxSource, NA)
  ## Model 4
  max_comp_log_lik_est = getMCLEmixed(compLikelihood_mixed_migInd, selSite, sels, gs[1], times[1], migs, sources)
  model     = c(model, "MODEL4")
  muLogLik  = c(muLogLik, MODEL4)
  maxLoc    = c(maxLoc, max_comp_log_lik_est[,1])
  maxSel    = c(maxSel, max_comp_log_lik_est[,2])
  maxG      = c(maxG, NA)
  maxTime   = c(maxTime, NA)
  maxMig    = c(maxMig, max_comp_log_lik_est[,5])
  maxSource = c(maxSource, max_comp_log_lik_est[,6])
  ## Model 5
  max_comp_log_lik_est = getMCLEmixed(compLikelihood_mixed_svInd, selSite, sels, gs, times, migs[1], sources)
  model     = c(model, "MODEL5")
  muLogLik  = c(muLogLik, MODEL5)
  maxLoc    = c(maxLoc, max_comp_log_lik_est[,1])
  maxSel    = c(maxSel, max_comp_log_lik_est[,2])
  maxG      = c(maxG, max_comp_log_lik_est[,3])
  maxTime   = c(maxTime, max_comp_log_lik_est[,4])
  maxMig    = c(maxMig, NA)
  maxSource = c(maxSource, NA)
}
OUT_DF = data.frame(model, muLogLik, maxLoc, maxSel, maxG, maxTime, maxMig, maxSource)
write.table(OUT_DF, file=paste0(DIR_OUT, "/", OUT_FNAME_PREFIX, ".csv"), sep=",", row.names=FALSE, col.names=TRUE, quote=FALSE)


####################################################################################
### Plot the composite log-likelihood profiles of each model across the scaffold ###
####################################################################################
if (is.na(MODEL_MAX_NAME)==FALSE){
  y_range = c((compLikelihood_ind_site - compLikelihood_neutral_site),
              (compLikelihood_mig_site - compLikelihood_neutral_site), 
              (compLikelihood_sv_site - compLikelihood_neutral_site), 
              (compLikelihood_mixed_migInd_site - compLikelihood_neutral_site),
              (compLikelihood_mixed_svInd_site - compLikelihood_neutral_site))
  y_range = y_range[!is.na(y_range)]
  plot_range = range(y_range)
  colors = c("black", "#4575b4", "#fb6a4a", "#74c476", "#fec44f")
  svg(paste0(DIR_OUT, "/", OUT_FNAME_PREFIX, ".svg"), width=7, height=10)
  par(mfrow=c(2,1))
  plot(x=c(min(selSite,na.rm=TRUE), max(selSite,na.rm=TRUE)), y=c(min(y_range,na.rm=TRUE)-(2*sd(y_range,na.rm=TRUE)), max(y_range,na.rm=TRUE)+(2*sd(y_range,na.rm=TRUE))), type="n",
       main="Likelihood profile across the scaffold",
       xlab = "Proposed position selected site",
       ylab = "Composite log-likelihood (model - neutral)")
  grid(lty=2, col="gray")
  ### plot the location of the QTL
  # abline(v=FREQ$POS[FREQ$LOD==max(FREQ$LOD,na.rm=TRUE)], lwd=10, col="gray")
  abline(v=FREQ$POS_BP[FREQ$LOD==max(FREQ$LOD,na.rm=TRUE)], lwd=10, col="gray")
  ### plot composite log-likelihoods
  lines(x=selSite, y=c(compLikelihood_ind_site          - compLikelihood_neutral_site), col=colors[1], type="b", lty=1, pch=0)
  lines(x=selSite, y=c(compLikelihood_mig_site          - compLikelihood_neutral_site), col=colors[2], type="b", lty=1, pch=6)
  lines(x=selSite, y=c(compLikelihood_sv_site           - compLikelihood_neutral_site), col=colors[3], type="b", lty=1, pch=2)
  lines(x=selSite, y=c(compLikelihood_mixed_migInd_site - compLikelihood_neutral_site), col=colors[4], type="b", lty=1, pch=3)
  lines(x=selSite, y=c(compLikelihood_mixed_svInd_site  - compLikelihood_neutral_site), col=colors[5], type="b", lty=1, pch=4)
  legend("topright",
         col=colors,
         pch=c(0,6,2,3,4),
         lty=1,
         legend=paste0("MODEL", c(1:5), ": loglik=", round(c(MODEL1, MODEL2, MODEL3, MODEL4, MODEL5),3)),
         bord=FALSE,
         cex=0.75)
  legend("bottomright", legend=c(paste0("Ne = ", round(Ne)),
                                 paste0("recom = ", round(rec, 5))), cex=0.75)
  ### plot LOD
  # par(new=TRUE)
  # plot(x=FREQ$POS, y=FREQ$LOD, type="b", pch=20, lty=2, main="Scaffold-specific manhattan plot", xlab="Position", ylab="GWAS log10(p-value)")
  plot(x=FREQ$POS_BP, y=FREQ$LOD, type="b", pch=20, lty=2, main="Scaffold-specific manhattan plot", xlab="Position", ylab="GWAS log10(p-value)")
  grid(lty=2, col="gray")
  dev.off()
}

# ### plot time
# ## Model 3
# mcle_sv = getMCLEsv_source(compLikelihood_sv, selSite, sels, gs, times, sources)
# idx_site = c(1:length(selSite))[selSite==mcle_sv[1]]

# compLike_sv_byTime = lapply(1 : length(times), function(time) {
#   sapply(1: length(sels), function(sel) {
#     sapply(1 : length(gs), function(g) {
#       compLikelihood_sv[[idx_site]][[sel]][[g]][[time]]
#     })
#   })
# })

# profileLike_time_sv = sapply(1: length(compLike_sv_byTime), function(i) {
#   max(unlist(compLike_sv_byTime[[i]]))
# })

# plot(times, profileLike_time_sv, type = "b", xlab = "Time",
#      ylab = "Profile composite log-likelihood", main = "Figure 2: Model 3")
# abline(v = mcle_sv[4], lty = 2, col = "red")
