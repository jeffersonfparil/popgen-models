### Simple Fst and PCA to test the magnitude of gene flow
### If:
### - gene flow is high then there is a good chance that the resistance alleles came from a single origin population
### - gene flow is low then there is a good chance that the resistance alleles arose independently in the resistant populations

fname_params = "input_fnames_params.txt"
params = read.delim(fname_params, header=TRUE)

i = 500
fname_geno = as.character(params$FNAME_FREQ_PER_SCAFFOLD[i])
fname_phen = as.character(params$FNAME_PHENO[i])
names_pops = unlist(strsplit(as.character(params$POPS[i]), ","))
idx_suscep = as.numeric(unlist(strsplit(as.character(params$ID_SUSCEP[i]), ",")))
names_resist = names_pops[!(c(1:length(names_pops))%in%idx_suscep)]
names_suscep = names_pops[idx_suscep]

GENO = read.csv(fname_geno)
PHEN = read.csv(fname_phen)

X = GENO[, c(colnames(GENO) %in% names_pops)]; rownames(X) = paste0("LOC_", 1:nrow(GENO))
y = PHEN$SURVIVAL
for (i in 1:ncol(X)){
    if (colnames(X)[i] %in% names_resist){
        colnames(X)[i] = paste0("RESIST\n", colnames(X)[i])
    } else {
        colnames(X)[i] = paste0("SUSCEP\n", colnames(X)[i])
    }
}


par(mfrow=c(2,1))
PCA = prcomp(t(X), scale=TRUE, center=TRUE)
biplot(PCA); grid()
hist(y)

