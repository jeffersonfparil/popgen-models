#!/bin/bash
### prepate input data from the MERGED_* herbicide resistance data:
### (1) putative QTL allele frequencies (i.e. the scaffolds with at least one selected locus and also at least 10 loci per scaffold)
### (2) sizes of each population

### set working directory
# cd "/home/student.unimelb.edu.au/jparil/Documents/POPULATION_GENETICS-resistance_development_in_weed_populations/popgen-models/src/"
# cd "/data/Lolium/Population_Genetics/POPGEN-MODELS/popgen-models/src/"
cd /data/Lolium/Population_Genetics/POPGEN-MODELS

### copy allele frequency data
# cp /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/MERGED_*_MAPQ20_BASQ20_MAF0.001_DEPTH50_ALLELEFREQ.csv .
for herbi in CLETH GLYPH SULFO TERBU
do
    cp /data/Lolium/Quantitative_Genetics/05_SYNC/${herbi}_MERGED_ALL_MAF0.001_DEPTH50_ALLELEFREQ.csv .
done

### copy phenotype data
# cp /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/MERGED_*.pheno .
for herbi in CLETH GLYPH SULFO TERBU
do
    cp /data/Lolium/Quantitative_Genetics/01_PHENOTYPES/${herbi}_MERGED_ALL.pheno .
done

### copy GWAS output
# cp /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/GWAS_PER_HERBI/MERGED_*_MAPQ20_BASQ20_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT.csv .
for herbi in CLETH GLYPH SULFO TERBU
do
    cp /data/Lolium/Quantitative_Genetics/06_GWAS/${herbi}_MERGED_ALL_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT.csv .
done

# ### using PER_HERBI_PER_POP BLASTOUT PEAK scaffolds instead of the peaks from the merged datasets
### using sequence peaks instead of the blast outputs
# cp /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/GWAS_ANALYSIS/BLASTOUT-*-SCAFFNAMES.txt .
for herbi in CLETH GLYPH SULFO TERBU
do
    # cut -f1 /data/Lolium/Quantitative_Genetics/06_GWAS/BLASTOUT-ORYZA-${herbi}_MERGED_ALL_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-GFF3_PEAKS.txt | \
        # cut -d: -f1 | sort | uniq > BLASTOUT-${herbi}-SCAFFNAMES.txt
    cut -d, -f1 /data/Lolium/Quantitative_Genetics/06_GWAS/${herbi}_MERGED_ALL_*-SEQ_PEAKS.csv | \
        sort | uniq > ${herbi}-SCAFFNAMES.txt
done


### copy the data for CLETH since it contains data for all the populations tested
### copy bam list and the correspoding mpileup for extracting the relevant populations from the mpileup and vcf file below
cp /data/Lolium/Quantitative_Genetics/04_MPILEUP/CLETH_MERGED_ALL_bam.list BAM_LIST.txt
cp /data/Lolium/Quantitative_Genetics/04_MPILEUP/CLETH_MERGED_ALL.mpileup MPILEUP.mpileup

### download poopolation for effective population size estimation (Ne) via nucleotide diversity (pi)
wget https://sourceforge.net/projects/popoolation/files/popoolation_1.2.2.zip
unzip popoolation_1.2.2.zip

### estimate remobination rate with [LDx](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0048588)
wget https://sourceforge.net/projects/ldx/files/LDx.pl
chmod +x LDx.pl

### set source code directory
SRC_DIR=/data/Lolium/Softwares/popgen-models/src

### extract scaffolds with peaks and filter the phenotype data to include only the SEAustralia whole population data
### INPUTS:
###@@@ (1) GWAS output
###@@@ (2) Allele frequency data
###@@@ (3) Phenotype data
###@@@ (4) Scaffold names from blast output (headerless text file)
### OUTPUTS:
###@@@ (1) Non-neutral allele frequency data (*-SELECTED_LOCI_DF.csv; HEADER: CHROM,POS,POB_BP,ALLELE,ALPHA,LOD,{ALLELE_FREQUENCIES_PERP_POPULATION...})
###@@@ (2) Filtered phenotype data (*-PHENOTYPES_DF.csv; HEADER:YEAR,SAMPLING,POP_GROUP,POP,NPOOLS,POOLSIZE,HERBICIDE,QUANTRES,SURVIVAL)
time \
parallel \
Rscript ${SRC_DIR}/0_1_prep.r \
    {}_MERGED_ALL_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT.csv \
    {}_MERGED_ALL_MAF0.001_DEPTH50_ALLELEFREQ.csv \
    {}_MERGED_ALL.pheno \
    {}-SCAFFNAMES.txt \
    {} \
    ::: CLETH GLYPH SULFO TERBU

### separate allele frequency data per scaffold
### INPUT:
###@@@ (1) f=${herbi}-SELECTED_LOCI_DF.csv
### OUTPUTS:
###@@@ (n) ${f%.csv*}-${scaff}.csv (HEADER: CHROM,POS,POB_BP,ALLELE,ALPHA,LOD,{ALLELE_FREQUENCIES_PERP_POPULATION...}))
for herbi in CLETH GLYPH SULFO TERBU
do
    # herbi=GLYPH
    f=${herbi}-SELECTED_LOCI_DF.csv
    tail -n+2 $f | cut -d"," -f1 | sort | uniq > scaff_names.list
    for scaff in $(cat scaff_names.list)
    do
        # scaff=$(cat scaff_names.list | head -n1)
        head -n1 $f > ${f%.csv*}-${scaff}.csv
        grep "$scaff" $f >> ${f%.csv*}-${scaff}.csv
    done
    rm scaff_names.list
done


### extract selected scaffolds, remove zero depth loci, and populations from the mpileup file
cat *-SELECTED_LOCI_DF.csv | cut -d, -f1 | sort | uniq | grep "scaffold" \
    > scaffold_names.txt ### extract scaffold names to be used in grepping below
time \
grep -f scaffold_names.txt MPILEUP.mpileup | grep -v -P "\t0\t" \
    > FILTERED.temp ### extract scaffold (row-wise)
cat *-PHENOTYPES_DF.csv | cut -d, -f2 | sort | uniq | grep "ACC" \
    > population_names.txt ### extract population names to be used in extracting the population indices below
grep -n -f population_names.txt BAM_LIST.txt | grep -v "Pool" | cut -d: -f1 \
    > column_idx.txt ### extract population indices
Rscript -e 'x = read.delim("column_idx.txt", header=FALSE); X = as.matrix(cbind((3*x)-2, (3*x)-1, (3*x)-0) + 3); out = c(1, 2, 3, as.vector(t(X))); write.table(out, file="column_idx_mpileup.txt", row.names=FALSE, col.names=FALSE, quote=FALSE)' ### extract population indices for mpileup extraction (column-wise)
time \
cut -f$(sed ':a;N;$!ba;s/\n/,/g' column_idx_mpileup.txt) FILTERED.temp \
    > FILTERED.mpileup ### extract populations (column-wise)
rm FILTERED.temp


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
### extract neutral scaffolds
### extract names of neutral scaffolds
echo 'args = commandArgs(trailingOnly=TRUE)
    # args = c("CLETH_MERGED_ALL_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT.csv",
    #          "GLYPH_MERGED_ALL_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT.csv",
    #          "SULFO_MERGED_ALL_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT.csv",
    #          "TERBU_MERGED_ALL_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT.csv")
    cleth = aggregate(LOD ~ CHROM, FUN=max, na.rm=TRUE, data=read.csv(args[1]))
    glyph = aggregate(LOD ~ CHROM, FUN=max, na.rm=TRUE, data=read.csv(args[2]))
    sulfo = aggregate(LOD ~ CHROM, FUN=max, na.rm=TRUE, data=read.csv(args[3]))
    terbu = aggregate(LOD ~ CHROM, FUN=max, na.rm=TRUE, data=read.csv(args[4]))
    merged = merge(merge(merge(cleth, glyph, by="CHROM"), sulfo, by="CHROM"), terbu, by="CHROM")
    merged = droplevels(merged[merged$CHROM != "Intercept", ])
    merged$MAX_LOD = apply(merged[,2:ncol(merged)], MARGIN=1, FUN=max)
    merged = merged[order(merged$MAX_LOD, decreasing=FALSE), ]
    neutral_scaffold_df = merged[merged$MAX_LOD < 1.00, ]; colnames(neutral_scaffold_df) = c("CHROM", "CLETH_LOD", "GLYPH_LOD", "SULFO_LOD", "TERBU_LOD", "MAX_LOD")
    neutral_scaffold_names = neutral_scaffold_df$CHROM
    write.table(neutral_scaffold_df, file="NEUTRAL-DF.txt", sep="\t", quote=FALSE, row.names=FALSE, col.names=TRUE)
    write.table(neutral_scaffold_names, file="NEUTRAL-SCAFFNAMES.txt", sep="\t", quote=FALSE, row.names=FALSE, col.names=FALSE)
' > extract_neutral_loci.r
Rscript extract_neutral_loci.r $(ls *_MERGED_ALL_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT.csv)
### extract mpileup data of neutral scaffolds
time \
grep -f NEUTRAL-SCAFFNAMES.txt MPILEUP.mpileup | grep -v -P "\t0\t" \
    > NEUTRAL.mpileup.temp ### extract scaffold (row-wise)
time \
cut -f$(sed ':a;N;$!ba;s/\n/,/g' column_idx_mpileup.txt) NEUTRAL.mpileup.temp \
    > NEUTRAL.mpileup ### extract populations (column-wise)
rm NEUTRAL.mpileup.temp
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


### Estimate Ne per poppulation using the top scaffolds --->> NOPE NOPE NOPE!!!! WE SHOULD USE NEUTRAL SCAFFOLDS!!!!
###@@@ extract mpileup per scaffold per population and compute Tajima's pi
###@@@&&& INPUTS:
###@@@&&& (1) population_names.txt
###@@@&&& (2) scaffold_names.txt
###@@@&&& (3) NEUTRAL.mpileup ###FILTERED.mpileup
###@@@&&& (4) MERGED_*.pheno - for pool sizes
###@@@&&& OUTPUT:
###@@@&&& (1) ${pop}_${scaffold}.pi (HEADERLESS: chrom, pos, nsnp, fraction_covered, Tajima's pi)
###@@@ NOTE: used entry with maximum pool size for Pool-seq
echo 'system("cat *_MERGED_ALL.pheno | head -n1 > MERGED_ALL.phen;
              for f in $(ls *_MERGED_ALL.pheno); do tail -n+2 $f >> MERGED_ALL.phen; done")
      dat = read.csv("MERGED_ALL.phen", header=TRUE)
      out = aggregate(POOL_SIZE ~ POPULATION, data=dat, FUN=max)
      write.table(out, file="MERGED_ALL.poolsizes", sep=",", row.names=FALSE, col.names=FALSE, quote=FALSE)
      ' > merge_max_poolsize_per_pop.r
Rscript merge_max_poolsize_per_pop.r
echo '#!/bin/bash
      i=$1
      j=$2
      # i=5
      # j=1
      pop=$(head -n${i} population_names.txt | tail -n1)
      column_idx=$(echo 1,2,3,$(echo "(($i * 3)-2)+3" | bc),$(echo "(($i * 3)-1)+3" | bc),$(echo "(($i * 3)-0)+3" | bc))
      # ### scaffold=$(head -n$j scaffold_names.txt | tail -n1)
      scaffold=$(head -n$j NEUTRAL-SCAFFNAMES.txt | tail -n1)
      # ### cut -f$(echo $column_idx) FILTERED.mpileup | grep "$scaffold" > ${pop}_${scaffold}.mpileup
      cut -f$(echo $column_idx) NEUTRAL.mpileup | grep "$scaffold" > ${pop}_${scaffold}.mpileup
      ###@@@ extract pool size (used entry with maximum pool size for Pool-seq)
      pool_size=$(grep $pop MERGED_ALL.poolsizes | cut -d, -f2)
      ###@@@ estimate Tajimas pi interpretted as pairwise diversity
      perl popoolation_1.2.2/Variance-sliding.pl \
          --measure pi \
          --input ${pop}_${scaffold}.mpileup \
          --output ${pop}_${scaffold}.pi.out \
          --pool-size ${pool_size} \
          --fastq-type illumina \
          --min-count 2 \
          --min-coverage 4 \
          --min-covered-fraction 0.001 \
          --window-size 50000 \
          --step-size 10000
      grep -v "na" ${pop}_${scaffold}.pi.out > ${pop}_${scaffold}.pi
      rm ${pop}_${scaffold}.pi.out*
' > popoolation_tajimas_pi.sh
chmod +x popoolation_tajimas_pi.sh
time \
parallel ./popoolation_tajimas_pi.sh {1} {2} ::: $(seq 1 $(cat population_names.txt | wc -l)) ::: $(seq 1 $(cat NEUTRAL-SCAFFNAMES.txt | wc -l))
### took ~2 hours 9 minutes on 32-core 283-GB RAM machine [2020-10-26]

###@@@ concatenate per population and calculate Ne based on average pi
###@@@ INPUT:
###@@@&&& (1) ${pop}_${scaffold}.pi (HEADERLESS: chrom, pos, nsnp, fraction_covered, Tajima's pi)
###@@@ OUTPUT:
###@@@&&& (1) ${pop}.ne ### effective population size computed uising arithmetic mean of Tajima's pi
mkdir PER_SCAFFOLD_PI/
echo 'args = commandArgs(trailingOnly=TRUE)
          # args = c("ACC01.pi")
          fname_pi = args[1]
          mutation_rate = as.numeric(args[2])
          dat = read.delim(args[1], header=FALSE)
          pi = mean(dat$V5, na.rm=TRUE)
          Ne = round(pi / (4*mutation_rate))
          write.table(Ne, file=sub("[.]pi", ".ne", fname_pi), row.names=FALSE, col.names=FALSE, quote=FALSE)
    ' > calc_Ne.r
for pop in $(cat population_names.txt)
do
    # pop=$(head -n1 population_names.txt)
    echo $pop
    cat ${pop}_*.pi > ${pop}.pi
    Rscript calc_Ne.r ${pop}.pi 10e-9 ### mutation rate based on arabidopsis which was noted in this paper: https://www.nature.com/articles/6800751 and this value is the median of the values used here Neve et al, 2003 : https://onlinelibrary.wiley.com/doi/abs/10.1046/j.0043-1737.2003.00358.x
    mv ${pop}*.pi PER_SCAFFOLD_PI/
done

### Estimate recombination rate (per bp per generation) across populations per top scaffold
### INPUTS:
###@@@ (1) full path and filename of the input bam file
###@@@ (2) mapping quality (PHRED) threshold
###@@@ (3) base quality (PHRED) threshold
###@@@ (4) minor allele frequency threshold
###@@@ (5) full path and filename of the reference genome
### OUTPUT:
###@@@ (1) {BAM%.bam*}-scaffold_*.recom
echo '#!/bin/bash
      BAM_PLUS_DIR=$1
      MAPQ=$2
      BASQ=$3
      MAF=$4
      REF=$5
      SRC_DIR=$6
  
      # cd "/data/Lolium/Population_Genetics/POPGEN-MODELS/test_bam_to_vcf"
      # cp ../scaffold_names.txt .
      # BAM_PLUS_DIR=/data/Lolium/Quantitative_Genetics/PoolGPAS_60SEAu/BK/SAM/ACC62_combined.bam
      # MAPQ=20
      # BASQ=20
      # MAF=0.001
      # REF=/data/Lolium/Genomics/SEQUENCES/DNA/REFERENCE_GENOMES/Reference.fasta
  
      samtools index ${BAM_PLUS_DIR}
      BAM=$(basename $BAM_PLUS_DIR)
      for i in $(seq 1 $(cat scaffold_names.txt | wc -l))
      do
          # i=3
          echo $i
          scaffold=$(head -n${i} scaffold_names.txt | tail -n1)
          samtools view -b ${BAM_PLUS_DIR} ${scaffold}  > ${BAM%.bam*}-${scaffold}.bam
          bcftools mpileup \
              --max-depth 10000 \
              --min-MQ ${MAPQ} \
              --min-BQ ${BASQ} \
              --fasta-ref ${REF} \
              ${BAM%.bam*}-${scaffold}.bam | \
          bcftools call -vm | \
          bcftools sort > ${BAM%.bam*}-${scaffold}.vcf
          samtools view -h -o ${BAM%.bam*}-${scaffold}.sam ${BAM%.bam*}-${scaffold}.bam
          min_depth=50
          max_depth=10000
          total_read_length=300
          min_intersection_depth=1
          ./LDx.pl \
              -l ${min_depth} \
              -h ${max_depth} \
              -s ${total_read_length} \
              -q ${BASQ} \
              -a ${MAF} \
              -i ${min_intersection_depth} \
              ${BAM%.bam*}-${scaffold}.sam \
              ${BAM%.bam*}-${scaffold}.vcf \
          > ${BAM%.bam*}-${scaffold}.ld
          # rm -Rf /tmp/bcftools*
          ### computing the recombination rate based on Clarke and Cardon (2005):https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1456135/pdf/GEN17142085.pdf
          ### with an additional trick of using the greatest distance among the most numberous pairs of distances then the computed recombination rate is divided by that distance
          Rscript ${SRC_DIR}/0_2_recom.r ${BAM%.bam*}-${scaffold}.ld
          ### cleanup
          mv ${BAM%.bam*}-${scaffold}.bam PER_SCAFFOLD_LD/
          mv ${BAM%.bam*}-${scaffold}.vcf PER_SCAFFOLD_LD/
          mv ${BAM%.bam*}-${scaffold}.sam PER_SCAFFOLD_LD/
          mv ${BAM%.bam*}-${scaffold}.ld PER_SCAFFOLD_LD/
      done
' > calc_recom.sh
chmod +x calc_recom.sh
mkdir PER_SCAFFOLD_LD
time \
parallel ./calc_recom.sh {} \
                         20 \
                         20 \
                         0.001 \
                         /data/Lolium/Genomics/SEQUENCES/DNA/REFERENCE_GENOMES/Reference.fasta \
                         ${SRC_DIR} \
                         ::: $(ls /data/Lolium/Quantitative_Genetics/PoolGPAS_60SEAu/BK/SAM/ACC*_combined.bam | grep -f population_names.txt)
### took ~2 hours on 32-core 283-GB RAM machine [2020-10-26]

### merge recombination rates per scaffold across populations
for scaffold in $(ls ACC*_*.recom | cut -d- -f2 | cut -d. -f1 | sort | uniq)
do
    # scaffold=$(ls ACC*_*.recom | cut -d- -f2 | cut -d. -f1 | sort | uniq | head -n1)
    echo $scaffold
    cat ACC*-${scaffold}.recom | Rscript -e 'mean(as.numeric(readLines("stdin")))' | cut -d' ' -f2 > ${scaffold}.recom
done

### clean-up
mv ACC*.mpileup PER_SCAFFOLD_LD/
mv ACC*-*.recom PER_SCAFFOLD_LD/


### Prepare "input_fnames_params.txt"
### Tab-delimited file: with the following field and header names:
### HERBI, SCAFFOLD, FNAME_FREQ_PER_SCAFFOLD, FNAME_PHENO, FNAME_F_ESTIMATE, POPS, ID_SUCEPT, ID_RESIST_GROUPING, NE, RECOM, NUMBINS
### Exclude scaffold with less than 10 loci:
mkdir EXCLUDED_SCFFOLDS/
for f in $(ls *-SELECTED_LOCI_DF-*.csv)
do
    if [ $(cat $f | wc -l) -lt 11 ]
    then
        mv $f EXCLUDED_SCFFOLDS/
    fi
done
### Prepare input parameter file
echo '
    ### HARDCODED LIST OF POPULATIONS AND RESISTANCE CLASSIFICATIONS
    CLETH_POPS = c("ACC06,ACC08,ACC09,ACC11,ACC38,ACC50", "ACC01,ACC05,ACC06,ACC27,ACC34,ACC36")
    CLETH_SUSC = c("1,5,6", "1,2,3")
    CLETH_RESG = c("(2,3),4", "(4,5),6")
    GLYPH_POPS = c("ACC04,ACC09,ACC24,ACC38,ACC39,ACC55", "ACC01,ACC02,ACC15,ACC18,ACC47,ACC50")
    GLYPH_SUSC = c("4,5,6", "1,2,3")
    GLYPH_RESG = c("(1,2),3", "4,(5,6)")
    SULFO_POPS = c("ACC03,ACC11,ACC12,ACC20,ACC53,ACC57", "ACC01,ACC14,ACC19,ACC21,ACC51,ACC53")
    SULFO_SUSC = c("1,4,6", "1,3,4")
    SULFO_RESG = c("(2,3),5", "2,(5,6)")
    TERBU_POPS = c("ACC04,ACC06,ACC07,ACC21,ACC26,ACC55", "ACC07,ACC09,ACC21,ACC26,ACC60,ACC62")
    TERBU_SUSC = c("3,4,5", "1,3,4")
    TERBU_RESG = c("(1,2),6", "2,(5,6)")

    for (herbi in c("CLETH", "GLYPH", "SULFO", "TERBU")){
        ### extract and prepare filenames and scaffold names
        scaffold_fnames = system(paste0("ls ", herbi, "-*scaffold*.csv"), intern=TRUE)
        scaffold_names = system(paste0("ls ", herbi, "-*scaffold*.csv | cut -d- -f3 | cut -d. -f1"), intern=TRUE)
        pheno_fname = system(paste0("ls ", herbi, "-PHENOTYPES_DF.csv"), intern=TRUE)
        neutral_fname = paste0("NEUTRAL_", herbi, "-F_estimate.csv")
        pop_names = eval(parse(text=paste0(herbi, "_POPS")))
        suscept_ids = eval(parse(text=paste0(herbi, "_SUSC")))
        resist_ids_grps = eval(parse(text=paste0(herbi, "_RESG")))
        Ne = c()
        for (pops in strsplit(pop_names, ",")){
            print(pops)
            Ne_list = c()
            for (pop in pops){
                Ne_list = c(Ne_list, read.delim(paste0(pop, ".ne"), header=FALSE)$V1)
            }
            print(Ne_list)
            Ne = c(Ne, mean(Ne_list))
        }
        recom = c()
        for (scaff in scaffold_names){
            recom = c(recom, read.delim(paste0(scaff, ".recom"), header=FALSE)$V1)
        }
        nbins = 100
        ### merge
        n = length(pop_names)
        m = length(scaffold_names)
        herbi = rep(herbi, times=n*m)
        scaffold_fnames = rep(scaffold_fnames, times=n)
        scaffold_names = rep(scaffold_names, times=n)
        pheno_fname = rep(pheno_fname, times=n*m)
        neutral_fname = rep(neutral_fname, times=n*m)
        pop_names = rep(pop_names, each=m)
        suscept_ids = rep(suscept_ids, each=m)
        resist_ids_grps = rep(resist_ids_grps, each=m)
        Ne = rep(Ne, each=m)
        recom = rep(recom, times=n)
        nbins = rep(nbins, times=n*m)
        out = data.frame(HERBI=herbi,
                        SCAFFOLD=scaffold_names,
                        FNAME_FREQ_PER_SCAFFOLD=scaffold_fnames,
                        FNAME_PHENO=pheno_fname,
                        FNAME_F_ESTIMATE=neutral_fname,
                        POPS=pop_names,
                        ID_SUSCEP=suscept_ids,
                        ID_RESIST_GROUPING=resist_ids_grps,
                        NE=Ne,
                        RECOM=recom,
                        NUMBINS=nbins)
        if (exists("OUT")==FALSE){
            OUT = out
        } else {
            OUT = rbind(OUT, out)
        }
    }
    ### output
    write.table(OUT, file="input_fnames_params.txt", sep="\t", row.names=FALSE, quote=FALSE)
' > prep_input_fnames_params.r
Rscript prep_input_fnames_params.r
rm prep_input_fnames_params.r
