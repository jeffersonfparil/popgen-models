#!/bin/bash

###########################################################################################
### COALESCENT-BASED LIKELIHOOD ESTIMATION OF DIFFERENT MODELS OF CONVERGENT ADAPTATION ###
###########################################################################################
### Using the composite-likelihood-based approach of Lee and Coop (2017)
### https://www.genetics.org/content/207/4/1591

###########################
### Models being tested ###
###########################
###@@@ (1) Model 1: independent mutation model - independent mutation of favourbale allele across all resistant populations
###@@@ (2) Model 2: geneflow model - favourable allele is shared between all resistant populations via geneflow
###@@@ (3) Model 3: standing genetic variation model - favourable allele is shared between all resistant populations from a common ancestor
###@@@ (4) Model 4: geneflow and independent mutation model - some population/s share the favourable allele via geneflow; while the other population/s obtained the favourable allele on the same locus via de novo mutation
###@@@ (5) Model 5: standing genetic variation and independent mutation model - some population/s share the favourable from a common ancestor; while the other population/s obtained the favourable allele on the same locus via de novo mutation

###################
### Assumptions ###
###################
###@@@ (1) Favourable allele is fixed in all the resistant populations
###@@@ (2) This favourable allele fixation is recent
###@@@ (3) The same selection pressure across resistant populations
###@@@ (4) The same rate of geneflow across resistant populations or groups of resistant populations
###@@@ (5) The same mutation rate across resistant populations

##############
### INPUTS ###
##############
###@@@ (1) Line number pointing to the entry in the input tab-delimited file listing the input files, Ne, recombination rates, etc (see below next input item for details) (NOTE taht we start at line 2 since line 1 is the header)
###@@@ (2) Tab-delimited file: "input_fnames_params.txt" with the following field and header names:
###@@@     HERBI, SCAFFOLD, FNAME_FREQ_PER_SCAFFOLD, FNAME_PHENO, FNAME_F_ESTIMATE, POPS, ID_SUCEPT, ID_RESIST_GROUPING, NE, RECOM, NUMBINS
###@@@ (3) working directory where all the input files are located
###@@@ (4) source code directory of the popgen-models.git repo, e.g. "/data/Lolium/Softwares/popgen-models/src"

####################
### MAIN OUTPUTS ###
####################
###@@@ (1) ${OUT_NEW_PREFIX}.csv - parameter estimates of the most likely model (HEADER: model, maxLoc, maxSel, maxG, maxTime, maxMig, maxSource)
###@@@ (2) ${OUT_NEW_PREFIX}.svg - likelihood profile and manhattan plot across the scaffold
###@@@ where: OUT_NEW_PREFIX=${herbi}--${scaffold}--$(sed "s/,/-/g" <(echo $pops))--$(sed "s/(/|/g" <(sed "s/)/|/g" <(sed "s/,/-/g" <(echo $id_resist_grouping))))

DIR=/data/Lolium/Population_Genetics/POPGEN-MODELS
SRC_DIR=/data/Lolium/Softwares/popgen-models/src

cd $DIR

time \
parallel ${SRC_DIR}/2_1_dmc_parallel.sh {} \
                                        input_fnames_params.txt \
                                        ${DIR} \
                                        ${SRC_DIR} \
                                        ::: $(seq 2 $(cat input_fnames_params.txt | wc -l))
### took ~1 hour 36 minutes on 32-core 283-GB RAM machine [2020-10-27]
### tests
# time ${SRC_DIR}/2_1_dmc_parallel.sh 8 input_fnames_params.txt $DIR $SRC_DIR ### ls CLETH--
# time ${SRC_DIR}/2_1_dmc_parallel.sh 699 input_fnames_params.txt $DIR $SRC_DIR ### ls TERBU--

### FINALLY MANUALLY ASSESS THE OUTPUT AND ONLY ACCOUNTING FOR SCAFFOLDS 
### WHERE THE GWAS PEAK IS RECAPITULATED WITH DMC
