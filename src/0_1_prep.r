##########################################################################################
### Prepare non-neutral allele frequency datasets plus the phenotype data ###
##########################################################################################
### NOTE: USE SIMULATED NEUTRAL DATA (with msms probably?) instead of empirically putative neutral sites from GWAS output

### INPUTS:
###@@@ (1) GWAS output
###@@@ (2) Allele frequency data
###@@@ (3) Phenotype data
###@@@ (4) Scaffold names from blast output (headerless text file)
### OUTPUTS:
###@@@ (1) Non-neutral allele frequency data (*-SELECTED_LOCI_DF.csv; HEADER: CHROM,POS,POB_BP,ALLELE,ALPHA,LOD,{ALLELE_FREQUENCIES_PERP_POPULATION...})
# ###@@@ (2) Neutral allele frequency data (*-NEUTRAL_LOCI_DF.csv; HEADER: CHROM,POS,POB_BP,ALLELE,ALPHA,LOD,{ALLELE_FREQUENCIES_PERP_POPULATION...})
###@@@ (3) Filtered phenotype data (*-PHENOTYPES_DF.csv; HEADER:YEAR,SAMPLING,POP_GROUP,POP,NPOOLS,POOLSIZE,HERBICIDE,QUANTRES,SURVIVAL)
args = commandArgs(trailingOnly=TRUE)
# setwd("/data/Lolium/Population_Genetics/POPGEN-MODELS")
# args = c("GLYPH_MERGED_ALL_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT.csv",
#          "GLYPH_MERGED_ALL_MAF0.001_DEPTH50_ALLELEFREQ.csv",
#          "GLYPH_MERGED_ALL.pheno", 
#          "BLASTOUT-GLYPH-SCAFFNAMES.txt",
#           "CLETH")
### input
fname_gwas = args[1]
fname_allelefreq = args[2]
fname_pheno = args[3]
fname_blastout = args[4]
herbi = args[5]
### load data
GWAS = read.csv(fname_gwas)
FREQ = read.csv(fname_allelefreq, header=FALSE)
PHEN = read.csv(fname_pheno, header=TRUE); colnames(PHEN) = c("SAMPLING", "POP", "POOL_ID", "YEAR", "POP_GROUP", "POOLSIZE", "HERBICIDE", "QUANTRES", "SURVIVAL")
### filter: only include ACROSS population or whole population datasets
idx_pop = (PHEN$SAMPLING == "ACROSS") & grepl("ACC", PHEN$POP)
FREQ = FREQ[, c(rep(TRUE, 3), idx_pop)]
PHEN = PHEN[idx_pop, ]
colnames(FREQ) = c("CHROM", "POS", "ALLELE", as.character(PHEN$POP))
FREQ$CHROM = as.character(FREQ$CHROM)
FREQ$ALLELE = as.character(FREQ$ALLELE)
### load scaffold names from blastout peaks
scaffold_names_selected = as.character(read.delim(fname_blastout, header=FALSE)$V1)
### extract positional and GWAS info. together with the allele frequencies for the non-neutral and neutral scaffolds
FUNCTION_EXTRACT_DATA = function(FREQ_DF, GWAS_DF, scaffold_names){
    # test: FREQ_DF=FREQ; GWAS_DF=GWAS; scaffold_names=scaffold_names_selected
    ### extract allele frequencies, positional information, and estimated allelic effects from these scaffolds
    idx_scaf = FREQ_DF$CHROM %in% scaffold_names
    FREQ_DF = FREQ_DF[idx_scaf, ]
    idx_scaf = GWAS_DF$CHROM %in% scaffold_names
    GWAS_DF = GWAS_DF[idx_scaf, ]
    ### extract only the allele with the highest estimated additive effect per locus
    ### this effectively collapses each loci into biallelic loci
    if (exists("out_temp")==TRUE){rm("out_temp")}
    GWAS_DF$LOCUS = paste0(GWAS_DF$CHROM, ":", GWAS_DF$POS)
    for (loc in unique(GWAS_DF$LOCUS)) {
        # loc = unique(GWAS_DF$LOCUS)[1]
        gwas = GWAS_DF[GWAS_DF$LOCUS==loc, ]
        gwas = gwas[order(gwas$ALPHA, decreasing=TRUE), ]
        chrom = as.character(gwas$CHROM[1])
        pos = gwas$POS[1]
        allele = as.character(gwas$ALLELE[1])
        if (exists("out_temp")==FALSE) {
            out_temp = FREQ_DF[(FREQ_DF$CHROM==chrom) & (FREQ_DF$POS==pos) & (FREQ_DF$ALLELE==allele), ]
        } else {
            out_temp = rbind(out_temp, FREQ_DF[(FREQ_DF$CHROM==chrom) & (FREQ_DF$POS==pos) & (FREQ_DF$ALLELE==allele), ])
        }
    }
    ### add re-mapped positional information
    ### simulate (make-up) scaffold positions in cM-ish by setting that LD is completely broken at 100kb?!?!?!?
    ### Nope! I'm not really 100% sure if it's not really relevant to have pos in cM
    ### It seems like I just need consecutive numbers from 0 to 1
    ### so I'm mapping positions per scaffold into the 0 to 1 range
    if (exists("OUT")==TRUE){rm("OUT")}
    for (scaff in unique(out_temp$CHROM)){
        # scaff = unique(out_temp$CHROM)[1]
        sub = out_temp[out_temp$CHROM == scaff, ]
        colnames(sub)[1:3] = c("CHROM", "POS", "ALLELE")
        min_pos = min(sub$POS)
        max_pos = max(sub$POS)
        sub$REMAPPED = (sub$POS - min_pos) / (max_pos - min_pos)
        gwas = GWAS[(as.character(GWAS$CHROM)==scaff), ]
        merged = merge(gwas, sub, by=c("CHROM", "POS", "ALLELE"))
        out = data.frame(CHROM=merged$CHROM,
                            POS=merged$REMAPPED,
                            POS_BP=merged$POS,
                            ALLELE=merged$ALLELE,
                            ALPHA=merged$ALPHA,
                            LOD=merged$LOD)
        out = cbind(out, merged[, 8:(ncol(merged)-1)])
        out = out[order(out$POS, decreasing=FALSE), ]
        if (exists("OUT")==FALSE){
            OUT = out
        } else {
            OUT = rbind(OUT, out)
        }
    }
    return(OUT)
}
SEL_DF = FUNCTION_EXTRACT_DATA(FREQ_DF=FREQ, GWAS_DF=GWAS, scaffold_names=scaffold_names_selected)
# NEU_DF = FUNCTION_EXTRACT_DATA(FREQ_DF=FREQ, GWAS_DF=GWAS, scaffold_names=scaffold_names_neutral)
### save output
fname_sel = paste0(herbi, "-SELECTED_LOCI_DF.csv")
# fname_neu = paste0(herbi, "-NEUTRAL_LOCI_DF.csv")
fname_phe = paste0(herbi, "-PHENOTYPES_DF.csv")
write.table(SEL_DF, file=fname_sel, row.names=FALSE, quote=FALSE, sep=",")
# write.table(NEU_DF, file=fname_neu, row.names=FALSE, quote=FALSE, sep=",")
write.table(PHEN,   file=fname_phe, row.names=FALSE, quote=FALSE, sep=",")
