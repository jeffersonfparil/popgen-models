#!/usr/bin/env Rscript

#The coalescent

#studyingfrom @Weber2008

###########################################
###										###
### HARDY & WEINBERG vs WRIGHT & FISHER ###
###										###
###########################################
	
	#############################################
	#The Hardy-Weinberg Equilibrium Model
	#	- infinite population size
	#	- random mating
	#	- no selection
	#	- no mutation
	#	- no migration
	#############################################

	# number homozygous founders or the number of alleles
		f=2
	# frequency of each alleles assuming equal contributions of each founding parent across generations for all time
		p=1/f
	# frequency of homozygotes (constant  until after the singularities evaporate)
		hom = f*(p^2)
	# frequency of heterozygotes (constant until the big rip)
		het = (factorial(f)/(factorial(2)*factorial(f-2))) * (2*p*(1-p))
	#plotting P(het) as a function of one allele frequency in a biallelic locus
		p = seq(0,1,by=0.01)
		het = 2*p*(1-p)
		plot(p,het, type="l", lwd=2, col="red", xlab="Allele Frequency", ylab="Heterozygote Frequency")
	#############################################


	#############################################
	#The Wright-Fisher Model
	#	- constant finite population size
	#	- random mating
	#	- no selection
	#	- non-overlapping (discrete) generation times
	#############################################

	simulate_WF_model <- function(n, m, x0, t, iter, PLOT=FALSE) {
		#simulate a biallelic locus on a haploid population
		#	n - number of individuals
		#	m - number of alleles
		#	x0 - initial haploid genotypes of n individuals in this 1 locus with m alleles
		#	t - number of generations to simulate
		#	iter - number of iterations or replications of one locus or number of independent loci to simulate

		P=matrix(NA, nrow=iter, ncol=t+1) #t generations plus generation 0
		if (PLOT==TRUE) {
			plot(x=c(0,t), y=c(0,1), type="n", xlab="Generations", ylab="Allele Frequency", main="Wright-Fisher Model")
			colors = palette(rainbow(iter))
		}
		for(i in 1:iter){
			# x0 = sample(0:(m-1), size=n, prob=rep(1/m, each=m), replace=TRUE) #for multiple loci
			x1=x0	#set the initial genotypes as the previous generation
			p=c(sum(x1[x1==1])/n) #allele frequency at generation 0
			for (j in 1:t){
				x2 = sample(x1, size=length(x1), replace=TRUE) #derive this generation from previous generation
				p = c(p, sum(x2[x2==1])/n) #calculate frequency of allele "1"
				x1 = x2	#set this generation as the previous generation for the nex iteration
			}
			P[i,] = p
			if (PLOT==TRUE) {
				lines(x=0:t, y=p, col=colors[i])
			}
		}

		#count the number of allele fixations (0 and 1 alleles)
		FIXATIONS=c()
		TIME_TO_FIXATION=c()
		for (i in 1:iter){
			test = P[i,]==1 | P[i,]==0 #test for fixation
			FIXATIONS = c(FIXATIONS, sum(test)>0)
			TIME_TO_FIXATION = c(TIME_TO_FIXATION, if(sum(test)==0){NA} else {(t+1)-sum(test)})
		}
		RESULTS = data.frame(ITER=1:iter, FIXATIONS, TIME_TO_FIXATION)		
		SIZE = n
		FIXED = mean(RESULTS$FIXATIONS, na.rm=TRUE)
		TIME = mean(RESULTS$TIME_TO_FIXATION, na.rm=TRUE)
		OUTPUT = c(SIZE, FIXED, TIME)
		return(OUTPUT)
	}

	# #input minimum and maximum population size range
	# args = commandArgs(trailing = TRUE)
	# MIN = args[1]
	# MAX = args[2]
	MIN=10
	MAX=100

	N = MIN:MAX # range of population sizes to test
	FINDINGS = matrix(NA, nrow=length(N), ncol=3)
	progressBar = txtProgressBar(min = 0, max = length(N), initial = 0, style=3, width=50)
	for (i in 1:length(N)){
		n=N[i]
		m=2
		x0 = sample(0:(m-1), size=n, prob=rep(1/m, each=m), replace=TRUE)
		t=10*n
		iter=10
		FINDINGS[i,] = simulate_WF_model(n, m, x0, t, iter, PLOT=TRUE)
		setTxtProgressBar(progressBar, i)
	}
	close(progressBar)

	FINDINGS = as.data.frame(FINDINGS)
	colnames(FINDINGS) = c("POP_SIZE", "FIXED", "TIME_TO_FIXATION")
	# write.csv(FINDINGS, file=paste0("WF-model-findings-", MIN, "-", MAX, ".csv"))

	# jpeg(paste0("WF-model-findings-", MIN, "-", MAX, ".jpeg"), width=800, height=700)
	plot(FINDINGS$POP_SIZE, FINDINGS$TIME_TO_FIXATION, xlab="Population Size", ylab="Time to Fixation")
	abline(lm(TIME_TO_FIXATION ~ POP_SIZE, data=FINDINGS))
	legend("bottomright", legend=paste0("CORR=", round(cor(FINDINGS$POP_SIZE, FINDINGS$TIME_TO_FIXATION), 2)))
	# dev.off()

	modTS = lm(TIME_TO_FIXATION ~ 0 + POP_SIZE, data=FINDINGS)
	slope = modTS$coefficient #generations per individual --> time to fixation: 1 individual added to the population increases the time to fixation by 1.37 generations

	#Findings:
	#	- Wright-Fisher model will always converge to fixation of either one of the alleles (It's like drift!)
	#	- likelihood of fixation of an allele is equal to its initial frequency
	#	- time to fixation is inversely correlated to the initial frequency

	#Analyzing time to fixation:
	# Since:	P(coalescence) = 1/n
	#			P(non-colesence after t generations) = (1-(1/n))^t
	# Then:		P(coalescence at gen t) = P(coalescence) * P(non-colesence after t-1 gens)
	#			P(coalescence at gen t) = (1/n) * (1-(1/n))^(t-1)
	# And for sufficiently large (realistic and non-infinitly large) n (or Ne):
	#			P(coalescence at gen t) = (1/n)exp(-(t-1)/n)
	#			which is the exponential distribution with:
	#				mean(t) = n; and
	#				sd(t) = n
	# Therefore the average time to coalescence is n; and this time vary +/- n standard deviations 
	#
	#
	#
	######################
	# Approximating P(coalescence) as the standard exponential distribution:
	# 			P(non-coalescence at time t) = (1 - 1/n)^t
	# where:
	#			e^x = lim {(1+x/m)^m} as m -> infinity
	# Now let:
	#			tau = t/n
	#			-> n = t/tau
	# then:
	#			P(non-coalescence at time t) = (1 - 1/n)^t
	#										 = (1 - tau/t)^t .....{(1+x/m)^m}
	# and for sufficiently large number of generations (t-> infinity):
	#			P(non-coalescence at time t) = lim {(1 - tau/t)^t} as t->inf
	#										 = e^(-tau)
	#										 = e^(-t/n)
	# Therefore:
	#			P(coalescence at gen t) = (1/n)e^(-(t-1)/n) {QED}
	#############################################
