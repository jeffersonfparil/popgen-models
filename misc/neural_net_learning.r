# Neural Networks Activation Functions

z = seq(-10,10, by=0.01) #can range from -inf to +inf

#logistic function
f.z.logit = 1/(1+exp(-z))

#hyperbolic tanget (sinh/cosh) function
f.z.hypta = sinh(z)/cosh(z)

plot(x=z, y=f.z.hypta, type="l", lwd=3, col="red", xlab="z", ylab="f(z)", main="Neural Network Activation Functions")
lines(x=z, y=f.z.logit, lty=2, lwd=3, col="blue")
legend("bottomright", legend=c("hyperbolic tangent", "logistic"), col=c("red", "blue"), lty=c(1,2), lwd=2)

#######################################################################
#Initializing the network with a bunch of random weights and biases
#######################################################################
#network specifications
n = 4	#number of input or training data (vectors of 1s and 0s)
m0 = 4	#number of nodes in the input layer (layer0)
m1 = 3	#number of nodes in the 1st hidden layer (layer1)
m2 = 3	#number of nodes in the 2nd hidden layer (layer2)
m3 = 3	#number of nodes in the 3rd hidden layer (layer3)
m4 = 2	#number of nodes in the output layer (layer4)
fz <- function(z) {1/(1+exp(-z))} #activation function (logistic or sigmoid function)
dfz <- function(z) {exp(z)/((1+exp(z))^2)}	#derivative of the activation funtion
#training data
TRAINING_INPUT = matrix(c(1,0,1,0,0,1,0,1,1,1,0,0,0,0,1,1), byrow=FALSE, nrow=m0, ncol=n) #each column represent a traning data point; and each row represent the 4 input nodes
EXPECTED_OUTPUT = matrix(c(1,0,1,0,0,1,0,1), byrow=FALSE, nrow=m4, ncol=n) #each column represent a traning data point; and each row represent the 2 output nodes
#layer 1 (input):
A1 = TRAINING_INPUT 
W1 = matrix(rnorm(n=m1*m0), nrow=m1, ncol=m0) 	#W1.jk refers to the weight of the connection between node k in layer 0 to node j in layer 1 (yes it's a bit confusing but you'll see how this works intuituvely with matrix multiplication)
												#and so k range up to m1 or the number of nodes in the next layer and j range up to m0 or the number of nodes in this layer
B1 = matrix(rep(rnorm(n=m1), times=n), nrow=m1, ncol=n)		#bias matrix for each node in this layer for each column or training data point
Z1 = W1%*%A1 + B1 								#activation function input with nrows=nodes in this layer and ncols=number of training data points
#layer 2:
A2 = fz(Z1)										#activation of the nodes in this layer
W2 = matrix(rnorm(n=m2*m1), nrow=m2, ncol=m1)
B2 = matrix(rep(rnorm(n=m2), times=n), nrow=m2, ncol=n)
Z2 = W2%*%A2 + B2
#layer 3:
A3 = fz(Z2)
W3 = matrix(rnorm(n=m3*m2), nrow=m3, ncol=m2)
B3 = matrix(rep(rnorm(n=m3), times=n), nrow=m3, ncol=n)
Z3 = W3%*%A3 + B3
#layer 4:
A4 = fz(Z3)
W4 = matrix(rnorm(n=m4*m3), nrow=m4, ncol=m3)
B4 = matrix(rep(rnorm(n=m4), each=n), nrow=m4, ncol=n)
Z4 = W4%*%A4 + B4
#layer 5 (output):
A5 = fz(Z4)
#######################################################################
#Backpropagation via gradient descent
#######################################################################
#cost function
C <- function(A) {1/m4*colSums((A-EXPECTED_OUTPUT)^2)}
C(A5)
#let's train the network one training dataset or vector at a time
SUM.dC.dW4=matrix(0, nrow=m4, ncol=m3); SUM.dC.db4=matrix(0, nrow=m4, ncol=1)
SUM.dC.dW3=matrix(0, nrow=m3, ncol=m2); SUM.dC.db3=matrix(0, nrow=m3, ncol=1)
SUM.dC.dW2=matrix(0, nrow=m2, ncol=m1); SUM.dC.db2=matrix(0, nrow=m2, ncol=1)
SUM.dC.dW1=matrix(0, nrow=m1, ncol=m0); SUM.dC.db1=matrix(0, nrow=m1, ncol=1)
for (i in 1:n){
	#partial derivatives of the cost with respect to Z (activation input)
	dC.dz4 = (fz(Z4[,i])-EXPECTED_OUTPUT[,i])*dfz(Z4[,i])
	dC.dz3 = t(W4)%*%dC.dz4 * dfz(Z3[,i])
	dC.dz2 = t(W3)%*%dC.dz3 * dfz(Z2[,i])
	dC.dz1 = t(W2)%*%dC.dz2 * dfz(Z1[,i])
	#partial derivatives of the cost with respect to the weights
	dC.dW4 = dC.dz4%*%t(A4[,i])
	dC.dW3 = dC.dz3%*%t(A3[,i])
	dC.dW2 = dC.dz2%*%t(A2[,i])
	dC.dW1 = dC.dz1%*%t(A1[,i])
	#partial derivatives of the cost with respect to the biases
	dC.db4 = dC.dz4
	dC.db3 = dC.dz3
	dC.db2 = dC.dz2
	dC.db1 = dC.dz1
	#OUT
	SUM.dC.dW4 = SUM.dC.dW4 + dC.dW4; SUM.dC.db4 = SUM.dC.db4 + dC.db4
	SUM.dC.dW3 = SUM.dC.dW3 + dC.dW3; SUM.dC.db3 = SUM.dC.db3 + dC.db3
	SUM.dC.dW2 = SUM.dC.dW2 + dC.dW2; SUM.dC.db2 = SUM.dC.db2 + dC.db2
	SUM.dC.dW1 = SUM.dC.dW1 + dC.dW1; SUM.dC.db1 = SUM.dC.db1 + dC.db1
}

#updating the weights and biases
nu = 10

W1.new = W1 - ((nu/n)*SUM.dC.dW1)
W2.new = W2 - ((nu/n)*SUM.dC.dW2)
W3.new = W3 - ((nu/n)*SUM.dC.dW3)
W4.new = W4 - ((nu/n)*SUM.dC.dW4)

B1.new = B1 - ((nu/n)*matrix(rep(SUM.dC.db1, times=n), nrow=m1, ncol=n))
B2.new = B2 - ((nu/n)*matrix(rep(SUM.dC.db2, times=n), nrow=m2, ncol=n))
B3.new = B3 - ((nu/n)*matrix(rep(SUM.dC.db3, times=n), nrow=m3, ncol=n))
B4.new = B4 - ((nu/n)*matrix(rep(SUM.dC.db4, times=n), nrow=m4, ncol=n))

#checking the trained network
A1 = TRAINING_INPUT
A2.new = fz(W1.new%*%A1 + B1.new)
A3.new = fz(W2.new%*%A2.new + B2.new)
A4.new = fz(W3.new%*%A3.new + B3.new)
A5.new = fz(W4.new%*%A4.new + B4.new)

sum(C(A5))
sum(C(A5.new))


###################################
###								###
### BUILDING A FLEXIBLE NETWORK ###
###								###
###################################

nI = 100	#number of nodes in the input layer
nO = 10		#number of nodes in the output layer
nL = 5		#number of hidden layers
nNL = 20	#number of nodes in each hidden layer

initialize <- function(nI, nO, nL, nNL){
	OUT = c()
	#weights from the input to the first hidden layer and biases into the first hidden layer
	OUT = list(OUT, W12 = matrix(rnorm(nNL*nI), nrow=nNL, ncol=nI))
	OUT = list(OUT, b2 = matrix(rnorm(nNL), nrow=nNL, ncol=1))
	#weights and biases across the hidden layers
	for (i in 2:nL) {
		eval(parse(text=paste0("OUT = list(OUT, W", i, i+1, "=matrix(rnorm(nNL*nNL), nrow=nNL, ncol=nNL))")))
		eval(parse(text=paste0("OUT = list(OUT, b", i+1, "=matrix(rnorm(nNL), nrow=nNL, ncol=1))")))
	}
	#weights from the last hidden layer to the output layer and biases into the output layer
	eval(parse(text=paste0("OUT = list(OUT, W", nL+1, nL+2, "=matrix(rnorm(nO*nNL), nrow=nO, ncol=nNL))")))
	eval(parse(text=paste0("OUT = list(OUT, b", nL+2, "=matrix(rnorm(nNL), nrow=nO, ncol=1))")))
	#output
	return(OUT)
}

INPUT = 

#halt for now --> I urgently need simulate GWAlpha on different pooling frameworks by simulating genotypes and phenotypes with adjstable genetic architectures
