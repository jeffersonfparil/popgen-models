#!/bin/bash

cd /data/Lolium/Softwares/
wget https://www2.unil.ch/popgen/softwares/quantinemo/files/quantinemo_linux.zip
unzip quantinemo_linux.zip; rm quantinemo_linux.zip
cd quantinemo_linux
./quantinemo -h
mv quantinemo.ini test.ini
cat test.ini
./quantinemo test.ini
