#exploring statistical models and understading how they are built and estimated semi-manually

###########################################################################################################
###########################################################################################################
###
### ORDINARY LEAST SQUARES
###
###########################################################################################################
###########################################################################################################

##################
### UNIVARIATE ###
################## CONTINUOUS
#simulate data: y = a + xb
a = 10 													#y-intercept
n = 50													#number of paired x-y observations
max.x = 10												#maximum value of x tested
x = sample(seq(0,max.x,by=0.1), size=50, replace=TRUE) 	#continuous variable x
b = 2													#effect of x
e = rnorm(n=n, mean=0, sd=max.x/3)						#error effects with mean 0 and sd 1
y = a+(x*b)+e
hist(y,nclass=10)

#R-automated modeling
mod = lm(y~x)						#y = a + bx + e
pred.a = coef(mod)[1]
pred.b = coef(mod)[2]; (b-pred.b)*100/b
Ve = anova(mod)$"Mean Sq"[2] 		#error variance of model
Vx = (anova(mod)$"Mean Sq"[1]-Ve)/1	#variance of X (from EMS: Vx = df*Vx + Ve)
plot(x=y , y=predict(mod), xlab="Observed y", ylab="Predicted y"); abline(lm(predict(mod)~y))
legend("topleft", legend=c(	as.expression(bquote(R^2 == .(round(summary(mod)$r.squared*100, 2))~"%")),
							as.expression(bquote(H^2 == .(round(Vx*100/(Vx+Ve), 2))~"%")),
							as.expression(bquote(Delta[a] == .(round((a-pred.a)*100/a, 2))~"%")),
							as.expression(bquote(Delta[b] == .(round((b-pred.b)*100/b, 2))~"%"))) )
#manual modelling
#given: y and X - estimate b
#use the normal equations: b_hat = (X'X)X'y
X = cbind(rep(1, times=length(x)), x)
estimates = solve(t(X)%*%X)%*%t(X)%*%y
pred.a = estimates[1]
pred.b = estimates[2]
	#how to anova?
	#SS.total= y'y - (1/n)y'Jy or = sum of squares - ( (square of sum)/divided by n )  = sum of squares - 1/n*(sum of products of each y with sum of y)
	SS.total = (t(y)%*%y) - ( (t(y)%*%matrix(1,nrow=length(y), ncol=length(y))%*%y) / n)
	#SS.error = SS(e = y - bx) = e'e = (y-bx)'(y-bx) = y'y - y'X(X'X)^{-1} X'y
	SS.error = (t(y)%*%y) - ( t(y)%*%X %*% solve(t(X)%*%X) %*% t(X)%*%y )
	#SS.sv = SS.total - SS.error or = y'y - (1/n)y'Jy - (y'y - y'X(X'X)^{-1} X'y) = y'X(X'X)^{-1}X'y - (1/n)y'Jy
	SS.sv = ( t(y)%*%X %*% solve(t(X)%*%X) %*% t(X)%*%y ) - ( (t(y)%*%matrix(1,nrow=length(y), ncol=length(y))%*%y) / n)


##################
### UNIVARIATE ###
################## CATEGORICAL
#simulate data: y = u + Xb
u = 10 			#population mean
m = 3			#number of categories of X
b = c(1,1,-2)	#effects of each category of X (should sum to zero becuase they are deviations from the mean)
r = 10			#number of replicted observations for each catogory of X
	categories = rep(c(1:m), each=r)
	X = matrix(1, nrow=(m*r), ncol=m)
	for(i in 1:m){X[X[,i]!=(categories-(i-1)),i] = 0} #only the correct categories per column are equal to 1
e = rnorm(n=m*r, mean=0, sd=1)	#error effects with mean 0 and sd 1
y = u+(X%*%b)+e

#R-automated modeling
mod = lm(y~0+as.factor(categories)) #force intercept at zero: y = 0 + x1b1 + x2b2 + ... + e
pred.b = coef(mod) - mean(y) 		#estimated effects of each category of X
Ve = anova(mod)$"Mean Sq"[2] 		#error variance of model
Vx = (anova(mod)$"Mean Sq"[1]-Ve)/r #variance of X (from EMS: Vx = r*Vx + Ve)
plot(x=b , y=pred.b); abline(lm(pred.b~b))
legend("topleft", legend=c( as.expression(bquote(rho == .(round(cor(b,pred.b)*100, 2))~"%")), 
							as.expression(bquote(R^2 == .(round(summary(mod)$r.squared*100, 2))~"%")),
							as.expression(bquote(H^2 == .(round(Vx*100/(Vx+Ve), 2))~"%"))) )
#manual modelling
#given: y and X - estimate b
#use the normal equations: b_hat = (X'X)X'y
pred.b = solve(t(X)%*%X)%*%t(X)%*%y
pred.b = pred.b - mean(y)


	#####################
	#Note:
	mod = anova(y~as.factor(categories))
	summary(mod) #is equivalent to
	X[,1] = 1
	View(X)
	solve(t(X)%*%X)%*%t(X)%*%y
	#####################

###########################################################################################################



###########################################################################################################
###########################################################################################################
###
### MIZED LINEAR MODELS
###
###########################################################################################################
###########################################################################################################

###########################################################################################################



###########################################################################################################
###########################################################################################################
### 
### CONTINUOUS versus BINARY PHENOTYPES in CORRELATION ANALYSIS
###
###########################################################################################################
###########################################################################################################
#20180607

#simulate data
n=100 	#number of individuals
m=4		#number of phenotype types?
	continuous.temp1 = rnorm(n, mean=0, sd=1)
	continuous.temp2 = continuous.temp1 + rnorm(n, mean=0, sd=1) #phenotype 2 is positively correlated with phenotype 1
	continuous.temp3 = rnorm(n, mean=0, sd=1)
	continuous.temp4 = (1-continuous.temp3) + rnorm(n, mean=0, sd=1) #phenbotype 4 is negatively correlated with phenotype 3
	continuous.temp = c(continuous.temp1, continuous.temp2, continuous.temp3, continuous.temp4)
continuous = (continuous.temp - min(continuous.temp))/(max(continuous.temp) - min(continuous.temp))
binary = round(continuous+rnorm(length(continuous), mean=0, sd=0.01), 0) #with some error effects ~N(0,0.1)

Y1 = matrix(continuous, nrow=n, ncol=4, byrow=FALSE)
Y2 = matrix(binary, nrow=n, ncol=4, byrow=FALSE)

#build logistic model
#nls seem to be better than glm bimomial link logit and I understand what's happenin g much better!
mod2 = nls(binary ~ 1/(1 + exp(-a*continuous)), start=list(a=1), control=nls.control(maxiter=1000))
pred = 1/(1+exp(-coef(mod2)*continuous))
cor(pred, binary)

#compare correlations using binary and continuous phenotypes
cor(Y1) #continuous
cor(Y2)	#binary

#plot!
#here's the proof that using a continuous phenotype is more powerful as detecting the covariations than binary phenotypes!
#lower correlations and R^2 when there were in fact correlation!
par(mfrow=c(3,4))
for(i in 1:3){
	for(j in (i+1):4){
		#continuous
		m = lm(Y1[,j] ~ Y1[,i])
		plot(x=Y1[,i], y=Y1[,j], main=paste0("Continuous ",i,"x",j)); abline(m)
		legend("bottomleft", legend=c(  as.expression(bquote(cor == .(round(cor(x=Y1[,i], y=Y1[,j]),2)))),
										as.expression(bquote(m == .(round(coef(m)[2],2)))),
										as.expression(bquote(pvalue == .(round(summary(m)$coefficients[2, 4],2)))),
										as.expression(bquote(R^2 == .(round(summary(m)$r.squared,2))))
										))
		#binary
		m = lm(Y2[,j] ~ Y2[,i])
		plot(x=Y2[,i], y=Y2[,j], main=paste0("Binary ",i,"x",j)); abline(m)
		legend("bottomleft", legend=c(  as.expression(bquote(cor == .(round(cor(x=Y2[,i], y=Y2[,j]),2)))),
										as.expression(bquote(m == .(round(coef(m)[2],2)))),
										as.expression(bquote(pvalue == .(round(summary(m)$coefficients[2, 4],2)))),
										as.expression(bquote(R^2 == .(round(summary(m)$r.squared,2))))
										))
	}
}
###########################################################################################################



###########################################################################################################
