#!/bin/bash
### Simulate population frequencies across time
### ASSUMPTIONS:
### (1) hermaphroditic
### (2) biallelic loci 
### (3) symmetric mutation rates 
### (4) selection disrupts zygote to reproductive adult survival 
### (5) discrete non-overlapping generations
### INPUTS:
### (1) GWAS output files: i.e. MERGED_${herbi}_MAPQ20_BASQ20_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT.csv for herbi in CLETH GLYPH SULFO TERBU NEUTRAL
### (?) synchronized pileup file: 60SEAus_pop_sizes.csv
### (?) population sizes file: 60SEAus_pop_sizes.csv
### OUTPUTS:
### ()

### NOTE: 2020-05-25
### MAY SUPERSCEED THE OUTPUT AND EVERYTHING IN "01_empirical_summstats.sh"
### SINCE THE SUMMSTATS WE ONLY NEED ARE THE ALLELE FREQUENCIES!

### set working directory
cd /data/Lolium/Population_Genetics/POPGEN-MODELS/
cp /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/GWAS_PER_HERBI/MERGED_*_MAPQ20_BASQ20_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT.csv .
cp /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/*_ALLELEFREQ.csv .
cp /data/Lolium/Quantitative_Genetics/PoolGPAS_MERGED/03_MERGED_DATA/*.pheno .

### (1) extract non-neutral and neutral loci to from the empirically inferred allelic effects of the Pool-GPAS experiments
R
herbi = "CLETH"

print("PREPARING TARGET MATRIX (i.e. the allele frequencies):")
n_sel = 100 ### number of non-neutral alleles (highest effects)
n_neu = 100 ### number of neutral alleles (effects = 0)
GWAS = read.csv(paste0("MERGED_", herbi, "_MAPQ20_BASQ20_MAF0.001_DEPTH50-MIXEDREML_USER_DEFINED_COVARIATE-OUTPUT.csv"))
GWAS = GWAS[GWAS$CHROM != "Intercept", ] ### remove the intercept
GWAS$LOCUS = paste(GWAS$CHROM, GWAS$POS, sep="-")
GWAS$ABS_EFFECT = abs(GWAS$ALPHA)
AGG = aggregate(ABS_EFFECT ~ LOCUS, data=GWAS, FUN=max)

### non-neutral loci (selected for)
AGG = AGG[order(AGG$ABS_EFFECT, decreasing=TRUE), ]
LOCI_SEL = matrix(unlist(strsplit(AGG$LOCUS[1:n_sel],  "-")), ncol=2, byrow=TRUE)
    LOCI_SEL = as.data.frame(LOCI_SEL)
    colnames(LOCI_SEL) = c("CHROM", "POS")
    LOCI_SEL$POS = as.numeric(as.character(LOCI_SEL$POS))
    LOCI_SEL = merge(LOCI_SEL, GWAS[,1:ncol(GWAS)], by=c("CHROM", "POS"))
    ### converting the loci into biallelic by excluding low frequency alleles
    for (locus in levels(as.factor(LOCI_SEL$LOCUS))){
        # locus = levels(as.factor(LOCI_SEL$LOCUS))[1]
        sub = LOCI_SEL[LOCI_SEL$LOCUS==locus, ]
        sub = sub[order(sub$FREQ, decreasing=TRUE), ]
        sub = sub[1:2, ]
        sub = sub[order(sub$ALPHA, decreasing=FALSE), ]
        sub$ALLELE_BIN = c(0, 1)
        if (exists("temp_out")==FALSE){
            temp_out = sub
        } else {
            temp_out = rbind(temp_out, sub)
        }
    }
    LOCI_SEL = temp_out; rm(temp_out)
    LOCI_SEL = LOCI_SEL[order(LOCI_SEL$ALLELE), 1:ncol(LOCI_SEL)]
    LOCI_SEL = LOCI_SEL[order(LOCI_SEL$POS), ]
    LOCI_SEL = LOCI_SEL[order(LOCI_SEL$CHROM), ]

### neutral loci
AGG = AGG[order(AGG$ABS_EFFECT, decreasing=FALSE), ]
LOCI_NEU = matrix(unlist(strsplit(AGG$LOCUS[1:n_sel],  "-")), ncol=2, byrow=TRUE)
    LOCI_NEU = as.data.frame(LOCI_NEU)
    colnames(LOCI_NEU) = c("CHROM", "POS")
    LOCI_NEU$POS = as.numeric(as.character(LOCI_NEU$POS))
    LOCI_NEU = merge(LOCI_NEU, GWAS[,1:ncol(GWAS)], by=c("CHROM", "POS"))
    ### converting the loci into biallelic by excluding low frequency alleles
    for (locus in levels(as.factor(LOCI_NEU$LOCUS))){
        # locus = levels(as.factor(LOCI_NEU$LOCUS))[1]
        sub = LOCI_NEU[LOCI_NEU$LOCUS==locus, ]
        sub = sub[order(sub$FREQ, decreasing=TRUE), ]
        sub = sub[1:2, ]
        sub = sub[order(sub$ALPHA, decreasing=FALSE), ]
        sub$ALLELE_BIN = c(0, 1)
        if (exists("temp_out")==FALSE){
            temp_out = sub
        } else {
            temp_out = rbind(temp_out, sub)
        }
    }
    LOCI_NEU = temp_out; rm(temp_out)
    LOCI_NEU = LOCI_NEU[order(LOCI_NEU$ALLELE), 1:ncol(LOCI_NEU)]
    LOCI_NEU = LOCI_NEU[order(LOCI_NEU$POS), ]
    LOCI_NEU = LOCI_NEU[order(LOCI_NEU$CHROM), ]

### load allele frequencies and extract the loci we want and remove the within population datasets
FREQ = read.csv(paste0("MERGED_", herbi, "_MAPQ20_BASQ20_MAF0.001_DEPTH50_ALLELEFREQ.csv"), header=FALSE)
idx_freq_df = c(TRUE, TRUE, TRUE, read.csv(paste0("MERGED_", herbi, ".pheno"), header=FALSE)$V2 == "ACROSS") ### include the first 3 columns: CHROM, POS, and ALLELE columns
FREQ = FREQ[, idx_freq_df]

### convert to biallelic by adjusting the allele frequencies
### non-neutral loci allele frequencies
FREQ_SEL = merge(data.frame(V1=LOCI_SEL$CHROM, V2=LOCI_SEL$POS, V3=LOCI_SEL$ALLELE, ALLELE_BIN=LOCI_SEL$ALLELE_BIN), FREQ, by=c("V1", "V2", "V3"))
    FREQ_SEL$LOCUS = paste0(FREQ_SEL$V1, "-", FREQ_SEL$V2)
    for (i in 1:length(unique(FREQ_SEL$LOCUS))){
        # i = 1
        locus = unique(FREQ_SEL$LOCUS)[i]
        freq_slice = FREQ_SEL[FREQ_SEL$LOCUS==locus, 5:(ncol(FREQ_SEL)-1)]
        colsums_slice = colSums(freq_slice)
        FREQ_SEL[FREQ_SEL$LOCUS==locus, 5:(ncol(FREQ_SEL)-1)] = freq_slice / matrix(rep(colsums_slice, times=2), nrow=2, byrow=TRUE)
    }
### neutral loci allele frequencies
FREQ_NEU = merge(data.frame(V1=LOCI_NEU$CHROM, V2=LOCI_NEU$POS, V3=LOCI_NEU$ALLELE, ALLELE_BIN=LOCI_NEU$ALLELE_BIN), FREQ, by=c("V1", "V2", "V3"))
    FREQ_NEU$LOCUS = paste0(FREQ_NEU$V1, "-", FREQ_NEU$V2)
    for (i in 1:length(unique(FREQ_NEU$LOCUS))){
        # i = 1
        locus = unique(FREQ_NEU$LOCUS)[i]
        freq_slice = FREQ_NEU[FREQ_NEU$LOCUS==locus, 5:(ncol(FREQ_NEU)-1)]
        colsums_slice = colSums(freq_slice)
        FREQ_NEU[FREQ_NEU$LOCUS==locus, 5:(ncol(FREQ_NEU)-1)] = freq_slice / matrix(rep(colsums_slice, times=2), nrow=2, byrow=TRUE)
    }


### (2) simulate!
### Inputs (n populations, m loci, t generations):
### (1) initial allele frequencies (F: p x m)
### (2) symmetric mutation rate (u: m x 1)
### (3) migration rate (M: n x n; where Mij is the migration rate from pop_i to pop_j and each column sums up to 1; probably make this sparse)
### (4) selection intensities (s: n x 1)
### (5) initial population sizes (z: n x 1) --> incorporate population size shrinkage as affected by selection and define a constant fecundity?
### (6) constant uniform fecundity
### (7) GWAS-derived allelic effects (b: m x 1)
### Outputs:
### (1)


### For the i^th locus in the t^th generation
###@@@ (1) Calculate genotype fitnesses: w11_t, w12_t, and w22_t, as a function of the input parameter: selection
###@@@     y = ALLELEFREQ %*% allelic_effects; then simulate selection on a symmetric logistic curve (where selection is the slope at center of the cuve); making the fitness values (wij) ranging from 0 to 1
###@@@ (2) Calculate allele frequencies after selection: p_i_{t+1}_sel = p_i_t * ((p_i_t*w11)+(q_i_t*w12)) / w_hat_t
###@@@ (3) Calculate allele frequencies after mutation: p_i_{t+1}_mut = (p_i_{t+1}_sel*(1-u)) + (q_i_{t+1}_sel*(u))
###@@@ (4) Calculate the range of probabilities of getting j alleles at the next generation after mutation and migration as affected by population size:
###@@@     P(j_i alleles at t+1) = Binomial_pmf(n=2N_t, k=j_i, p=p_i_{t+1}_mut, q=q_i_{t+1}_mut)
###@@@ (5) Sample how many alleles (j_i) we get at the next generation using this range of binomial probabilities
###@@@     This is were the stochaticity of the simulation comes in (as a consequences of non-infinite population sizes)
###@@@ (6) Sample N_t * m_t migrants from the populations and add the j_i alleles to the adjacent populations
###@@@     Since we can only sample individual alleles (j_i) from the populations and we will be sampling them
###@@@     independently then no linkage is assumed between the loci we are simulating
### For each generation we have:
###@@@ (1) initial allele frequencies (F: p x m)
###@@@ (2) population size (n: p x 1)
###@@@ (3) fitnesses (W: p x m x 3)
###@@@ (4) allele frequencies after selection (F: p x m)
###@@@ (5) allele frequencies after mutation (F: p x m)
###@@@ (6) allele counts (C: p x m) and their probabilities (P: p x m) during drift sampling
###@@@ (7) allele frequencies after drift (F: p x m)
###@@@ (8) allele frequencies after migration (F = t(M) %*% F: p x m)
###@@@ (9) need to account for when population size is zero (i.e. starting population size is zero or everybody dies)
###@@@     then new individuals can migrate there

### number of populations and number of loci to simulate
p = ncol(FREQ_SEL) - 5 ### number of populations: the 5 extra columns are col1, 2, 3, 4, last-col: chrom, pos, allele, allele_bin, and locus, respectively
m = nrow(FREQ_SEL)/2   ### number of loci (biallelic) ---> represents only the allele with the highest positive effect pewr locus

### input paramter simulation function
param_sim = function(param_min, param_max, p, m, distn="unif") {
    # param_min = 0 ### minimum parameter value
    # param_max = 1 ### maximum parameter value
    # p = 60        ### number of populations (nrows)
    # m = 100       ### number of loci (ncols)
    # dist = "unif" ### distribution to sample the parameter values from
    if (distn=="unif"){
        ### random uniform
        param = matrix(runif(n=p*m, min=param_min, max=param_max), nrow=p, ncol=m)
    } else if (distn=="chi") {
        ### chi-square distributed
        param = matrix(rchisq(n=p*m, df=1), nrow=p, ncol=m)
            param = (param-min(param))/(max(param)-min(param))
            param = (param*(param_max-param_min)) + param_min
    } else if (distn=="norm") {
        ### normally distributed
        param = matrix(rnorm(n=p*m, mean=1, sd=1), nrow=p, ncol=m)
            param = (param-min(param))/(max(param)-min(param))
            param = (param*(param_max-param_min)) + param_min
    } else {
        print("Invalid 'distn'. Choose: 'unif', 'chi', or 'norm'.")
        param = NA
    }
    return(param)
}


### (1) initial allele frequencies
F_min = 0
F_max = 1
F = param_sim(param_min=F_min, param_max=F_max, p=p, m=m, distn="unif")

### (2) mutation rates
u_min = 1e-8
u_max = 1e-4
u = param_sim(param_min=u_min, param_max=u_max, p=1, m=m, distn="norm")[1,] ### p=1 since mutation rates are the same for all populations

### (3) migration rates
M_min = 0.0
M_max = 1e-3
M = param_sim(param_min=u_min, param_max=u_max, p=p, m=m, distn="chi")

### (4) relative proportion of the distance from the maximum possible phenotypic value (proportional to selection intensity)
s_min = 0.4 ### lower s : more intense selection; where s=0.01 means individuals with phenotypic values that is 1% away from the maximum possible phenotypic valus are selected 
s_max = 1.0 ### higher s : less intense selection; where s=1.0 means no selection
s = param_sim(param_min=s_min, param_max=s_max, p=p, m=1, distn="unif")[,1] ### m=1 since selection applies to individual genotypes

### (5) initial population sizes
z_min = 0
z_max = 1e4
z = round(param_sim(param_min=z_min, param_max=z_max, p=p, m=1, distn="norm")[,1]) ### m=1 because these are population sizes

### (6) fecundity
x = 1 ### number of offsprings that gets established in the next generation per individual

### (7) allelic effects
b0 = LOCI_SEL$ALPHA[LOCI_SEL$ALLELE_BIN==0]
b1 = LOCI_SEL$ALPHA[LOCI_SEL$ALLELE_BIN==1]


### (I) selection
### increase the allelic effects by factors of 10 until we get phenotypic range between -Inf and +Inf with max at least 10 because the logistic curve makes sense when the x-axis (phenotypic values) have a range greater that 0-1
y_min = sum(2*b0)
y_max = sum(2*b1)
while(y_max<10){ ### assumes y_max is a positive value
    b0 = b0 * 10
    b1 = b1 * 10
    y_min = sum(2*b0)
    y_max = sum(2*b1)
}
### set the minimum phenotypic values per population with advantageous fitness as a function of s, the relative proportion of the distance from the maximum possible phenotypic value
y_c = y_min + ((1-s)*(y_max-y_min))
### calulate mean phenotypes per genotype class per population assuming no linkage between loci
y11 = rowSums(F * matrix(rep(b1+b1, times=p), nrow=p, byrow=TRUE))
y10 = rowSums(F * matrix(rep(b1+b0, times=p), nrow=p, byrow=TRUE))
y00 = rowSums(F * matrix(rep(b0+b0, times=p), nrow=p, byrow=TRUE))
### calculate fitnesses per genotype class per population using Richards (1959) logistic curve
w11 = (1 + exp(y_c - y11))^-1
w10 = (1 + exp(y_c - y10))^-1
w00 = (1 + exp(y_c - y00))^-1
### calculate the mean fitness per population
W11 = matrix(rep(w11, times=m), ncol=m, byrow=FALSE)
W10 = matrix(rep(w10, times=m), ncol=m, byrow=FALSE)
W00 = matrix(rep(w00, times=m), ncol=m, byrow=FALSE)
W_hat = ((F^2)*(W11)) + ((2*F*(1-F))*W10) + (((1-F)^2)*W00)
### calculate the allele frequencies after selection
F_sel = (((F^2)*W11) + ((1-F)*(F)*W10)) / W_hat

###  (II) mutation (symmetric)
U = matrix(rep(u, times=p), nrow=p, byrow=TRUE)
F_mut = (F_sel*(1-U)) + ((1-F)*U)

### (III) drift
C = list()
for (i in 1:p){
    # i=1
    n_ind = z[i]
    j = 1:n_ind
    prob = ( factorial(n_ind)/(factorial(j)*factorial(n_ind-j)) ) ### huge numbers!!!! Cannnot handle it!!!
    combin()
    
}
