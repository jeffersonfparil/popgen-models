#!/bin/bash
### Using the scaffolds from the merged GWAS peaks blast hits per herbicide resistance trait
### we compute the summary statistics using NPSTAT
### INPUTS:
### (1) mpileup files: i.e. BLASTOUT-${herbi}-MPILEUP.mpileup for herbi in CLETH GLYPH SULFO TERBU NEUTRAL
###     remove loci with at least 1 missing read via: grep -v $'\t'0$'\t' BLASTOUT-${herbi}-MPILEUP.mpileup > BLASTOUT-${herbi}-MPILEUP_FILTERED.mpileup
### (2) population sizes file: 60SEAus_pop_sizes.csv
### (3) annontation file: wget http://185.45.23.197:5080/ryegrassdata/GENE_ANNOTATIONS/Lp_Annotation_V1.1.mp.gff3
### OUTPUTS:
### (5x60) NPSTAT-${herbi}-${pop}.stats fir herbi in CLETH GLYPH SULFO TERBU NEUTRAL for pop in ACC01 ACC02 ... ACC62

### set working directory
cd /data/Lolium/Population_Genetics/POPGEN-MODELS/

### extract population names and population sizes and the cutting coordinates for separating each populatiojn
Rscript -e "
    dat = read.csv('60SEAus_pop_sizes.csv');
    n = nrow(dat);
    slut = (c(1:n) * 3) + 3;
    init = slut - 2;
    cutout = paste(init, slut, sep='-')
    hapsizes = dat[,2] * 2
    write.table(cutout, 'cutout.temp', quote=FALSE, row.names=FALSE, col.names=FALSE)
    write.table(hapsizes, 'hapsizes.temp', quote=FALSE, row.names=FALSE, col.names=FALSE)
    write.table(dat[,1], 'popnames.temp', quote=FALSE, row.names=FALSE, col.names=FALSE)
"

### parallel execution per population across scaffolds
echo '#!/bin/bash
DIR_NPSTAT=$1
herbi=$2
i=$3  ### iterator across populations
window_size_bp=$4
minimum_coverage=$5
mpileup=$6
gff3=$7
# DIR_NPSTAT=/data/Lolium/Softwares/npstat/
# herbi=CLETH
# i=1
# window_size_bp=1000000
# minimum_coverage=50
# mpileup=BLASTOUT-${herbi}-MPILEUP_FILTERED.mpileup ### see input specifications above
# gff3=Lp_Annotation_V1.1.mp.gff3 ### see input specifications above

### extract names, haploid sizes and cutting coordinates
pop=$(head -n$i popnames.temp | tail -n1)
haploid_size=$(head -n$i hapsizes.temp | tail -n1)
cutme=$(head -n$i cutout.temp | tail -n1)
cut -f${cutme} ${mpileup} > ${herbi}-${pop}.temp
paste col123.temp ${herbi}-${pop}.temp > ${herbi}-${pop}.pileup
### iterate acrioss scaffolds
for scaff in $(cut -f1 ${herbi}-${pop}.pileup | sort | uniq)
do
    # scaff=$(cut -f1 ${herbi}-${pop}.pileup | sort | uniq | head -n1)
    echo ${scaff}
    grep $scaff ${herbi}-${pop}.pileup > ${herbi}-${pop}-${scaff}-SUB.pileup
    grep $scaff $gff3                  > ${herbi}-${pop}-${scaff}-SUB.gff3
    ${DIR_NPSTAT}/npstat \
        -n ${haploid_size} \
        -l ${window_size_bp} \
        -mincov ${minimum_coverage} \
        -nolowfreq 1 \
        -annot ${herbi}-${pop}-${scaff}-SUB.gff3 \
        ${herbi}-${pop}-${scaff}-SUB.pileup
done
### merge npstat output across scaffolds
head -n1 $(ls ${herbi}-${pop}-*-SUB.pileup.stats | head -n1) > NPSTAT-${herbi}-${pop}.stats
for f in $(ls ${herbi}-${pop}-*-SUB.pileup.stats)
do
    tail -n+2 $f >> NPSTAT-${herbi}-${pop}.stats
done
rm ${herbi}-${pop}*
' > npstat_parallel_per_pop.sh
chmod +x npstat_parallel_per_pop.sh

### execute per herbicide
for herbi in CLETH GLYPH SULFO TERBU NEUTRAL
do
    # herbi=CLETH
    DIR_NPSTAT=/data/Lolium/Softwares/npstat/
    window_size_bp=1000000
    minimum_coverage=50             ### NOTE!!! TEST IF INCREASING MINCOV TO 50 WOULD DRASTICALLY CHANGE THE SUMMSTATS!!!!
    popsizes=60SEAus_pop_sizes.csv
    gff3=Lp_Annotation_V1.1.mp.gff3 ### see input specifications above
    mpileup=BLASTOUT-${herbi}-MPILEUP_FILTERED.mpileup ### see input specifications above
    cut -f1,2,3 BLASTOUT-${herbi}-MPILEUP_FILTERED.mpileup > col123.temp
    time \
    parallel ./npstat_parallel_per_pop.sh \
                    ${DIR_NPSTAT} \
                    ${herbi} \
                    {} \
                    ${window_size_bp} \
                    ${minimum_coverage} \
                    ${mpileup} \
                    ${gff3} \
                    ::: $(seq 1 $(tail -n+2 $popsizes |  wc -l))
done #$herbi
rm *.temp
