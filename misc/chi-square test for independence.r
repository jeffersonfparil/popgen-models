#Chi-square test for independence

#say we have this count data (number of individuals) with two variables:
#	Freq. count of smoking in rows: Heavy, Never, Ocassional and Regular
#	Freq. count of excercise: Frequent, None and Some
# Now we want to test if the smoking habit is independent of exercise habit

library(MASS)
x = table(survey$Smoke, survey$Exer) 

# These are our observed counts ---> chi-square test computes counts! NOT FREQUENCIES!!!!
o = x

# Computing for the expected counts:
# E_{i,j} = (ni/n^2 * nj/n^2) * n

row = rowSums(x)
col = colSums(x)
e1=c()
for(i in row){
	for(j in col){
		e1=c(e1, i*j/(sum(x)^2))
	}
}
e = matrix(e1, nrow=length(row), byrow=TRUE) * sum(x)

# Computing the chi-square statistic
# SUM{ (O-E)^2 / E }
X2 = sum(((o-e)^2)/e)
p.value = pchisq(q=X2, df=((ncol(x)-1)*(nrow(x)-1)), lower.tail=FALSE)

#checking:
chisq.test(x)
chisq.test(x)$statistic == X2
chisq.test(x)$p.value ==p.value
