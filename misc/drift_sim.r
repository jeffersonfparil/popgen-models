#simple simulation of genetic drift on 1 biallelic locus

fftd <- function(N, p, g, r) {
    #INPUT:
    #N = population size
    #p = initial frequency of the reference allele of the biallelic locus
    #g = number of generations of random mating
    #r = number of replications
    OUT = matrix(NA, nrow=g+1, ncol=r)
    for (i in 1:r) {
        f=c(p)
        for (j in 1:g) {
            #random sampling from binomial distribution with size 2N (diploid) and probability of success equal to p
            f = c(f, rbinom(n=1, size=(2*N), prob=f[j])/(2*N))
        }
        OUT[,i] = f
    }
    colnames(OUT) = paste("REP", 1:r)
    rownames(OUT) = paste("GEN", 0:g)
    return(OUT)
}

#execute
N=10
p=0.5
g=20
r=10
DRIFT=fftd(N=N, p=p, g=g, r=r)

#plot
colors=RColorBrewer::brewer.pal(10, name="Set3")
plot(x=0, y=0, type="n", xlim=c(0,g), ylim=c(0,1), xlab="Generations", ylab="Allele Frequency")
for (k in 1:r) {
    lines(x=0:g, y=DRIFT[,k], col=colors[k], lwd=2)
}
legend("topleft", legend=paste0("2N = ", 2*N))
