#!/bin/bash

#paralellize non-factorial
parallel --link ./coalescent.r {1} {2} ::: $(seq 10 1000 10100) ::: $(seq 1000 1000 11000)

#concatenate output csv files
cat WF-model-findings-10-110.csv > WF-model-OUTPUT.csv
tail -n+2 WF-model-findings-*.csv >> WF-model-OUTPUT.csv