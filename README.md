# popgen-models

Calculating the likelihood of different evolutionary models using the coalescent theory-based method of Lee, Kristin M, and Graham Coop. 2017. “Distinguishing Among Modes of Convergent Adaptation Using Population Genomic Data.” https://doi.org/10.1534/genetics.117.300417. Refer to Kristin Lee's repository for more details: [https://github.com/kristinmlee/dmc](https://github.com/kristinmlee/dmc)

The output of the analysis of herbicide resistance evolution in _Lolium rigidum_ populations from South East Australia are found in [misc/Lrigidum_SEAu_2018/](https://gitlab.com/jeffersonfparil/popgen-models/-/tree/master/misc/Lrigidum_SEAu_2018).